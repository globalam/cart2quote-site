<?php
/**
 * @version   1.0 14.08.2012
 * @author    TonyEcommerce http://www.TonyEcommerce.com <support@TonyEcommerce.com>
 * @copyright Copyright (c) 2012 TonyEcommerce
 */

class Etheme_Buyshopconfig_Block_Adminhtml_Installtemplate_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form_builder = new Varien_Data_Form();
        $fieldset = $form_builder->addFieldset('action_fieldset', array('legend'=>Mage::helper('buyshopconfig')->__('Choose your action and click Submit')));

        $fieldset->addField('store_id', 'select', array(
            'name'      => 'store',
            'title'     => Mage::helper('cms')->__('Store View'),
            'label'     => Mage::helper('cms')->__('Store View'),
            'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            'note' =>'List of stores configured in administrator panel using default Magento features',
        ));
       
        $fieldset->addField('action', 'select', array(
            'name'      => 'action',
            'title'     => Mage::helper('cms')->__('Store View'),
            'label'     => Mage::helper('cms')->__('Action'),
            'values'    => array(array('value'=>'install','label'=>'install BuyShop theme','image'=>$this->getSkinUrl('buyshop/images/gnome_app_install.png')),
                                 array('value'=>'uninstall',  'label'=>'restore previous theme ','image'=>$this->getSkinUrl('buyshop/images/Uninstall-Tool-icon.png')),
            ),
            'note' => 'Install/Deinstall BuyShop theme'
        ));

        $form_builder->setMethod('post');
        $form_builder->setAction($this->getUrl('*/*/dispatch'));
        $form_builder->setUseContainer(true);
        $form_builder->setId('edit_form');
        $this->setForm($form_builder);
        
        return parent::_prepareForm();
    }
}

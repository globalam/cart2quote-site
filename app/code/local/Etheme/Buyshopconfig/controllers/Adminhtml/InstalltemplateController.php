<?php
/**
 * @version   1.0 14.08.2012
 * @author    TonyEcommerce http://www.TonyEcommerce.com <support@TonyEcommerce.com>
 * @copyright Copyright (c) 2012 TonyEcommerce
 */

class Etheme_Buyshopconfig_Adminhtml_InstalltemplateController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
        $this->loadLayout();
        $this->_addLeft($this->getLayout()->createBlock('core/text', 'leftside')->setText('<h2>Theme Maintenance</h2><h4>Add description later.</h4>'));
        $this->_addContent($this->getLayout()->createBlock('buyshopconfig/adminhtml_installtemplate_edit'));
        $this->_setActiveMenu('etheme');
        $this->getLayout()->getBlock('head')->addJs('etheme/buyshop/jquery-1.7.2.min.js')
            ->addJs('etheme/buyshop/noconflict.js')
            ->addJs('etheme/buyshop/jquery.dd.min.js')
            ->addJs('etheme/buyshop/admin.js');
        $this->getLayout()->getBlock('head')->addItem('skin_css', 'buyshop/css/dd.css')
            ->addItem('skin_css', 'buyshop/css/rewrite_system.css');
        $this->renderLayout();
	}

	public function dispatchAction()
    {
        $store = $this->getRequest()->getParam('store', 0);
        $action = $this->getRequest()->getParam('action', 'install');
        $this->getResponse()->setRedirect($this->getUrl("*/*/".$action."/store/".$store));
    }

    public function installAction()
    {
        $store = 0;
        Mage::getModel('buyshopconfig/content')->installTemplateResources();
        Mage::getModel('buyshopconfig/content')->installTemplateBlocks();
        Mage::getModel('buyshopconfig/content')->installTemplateConfig($store);
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('buyshopconfig')->__("Buyshop Template installed successfully. If you do not see the changes please clean the cache.<br /><b>ATTENTION!!</b> Log out from magento admin panel if you are logged in. That is required step for final theme installation. Also this will avoid 404 page in theme settings"));
        $this->getResponse()->setRedirect($this->getUrl("*/*/"));
    }

    public function uninstallAction()
    {
        $store = $this->getRequest()->getParam('store', 0);
        $scope=($store ? 'stores' : 'default');
        Mage::getConfig()->saveConfig('design/theme/default', 'default', $scope, $store);
        Mage::getConfig()->saveConfig('web/default/cms_home_page', 'home', $scope, $store);
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('buyshopconfig')->__('Buyshop Template uninstalled successfully. If you do not see the changes please clean the cache.'));
        $this->getResponse()->setRedirect($this->getUrl("*/*/"));
    }


}




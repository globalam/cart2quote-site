<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Checkout
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
require_once 'Mage/Checkout/controllers/OnepageController.php';

class XYZ_Checkout_OnepageController extends Mage_Checkout_OnepageController
{

    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost('billing', array());
            $data = $this->_filterPostData($postData);
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                if ($this->getOnepage()->getQuote()->isVirtual()) {
					$this->loadLayout('checkout_onepage_review');
                    $result['goto_section'] = 'review';
                    $result['update_section'] = array(
                        'name' => 'review',
                        'html' => $this->_getReviewHtml()
                    );
					$result['allow_sections'] = array('shipping');
					
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                	$this->loadLayout('checkout_onepage_review');	
                	$this->saveShippingMethodAction();
                	
					$this->getOnepage()->saveShippingMethod($this->getRequest()->getPost('shipping_method', 'freeshipping_freeshipping'));
                	
					$result['goto_section'] = 'review';
                    $result['update_section'] = array(
                        'name' => 'review',
                        'html' => $this->_getReviewHtml()
                    );

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
	

	 public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);

            if (!isset($result['error'])) {
		  		try {
		  			$this->saveShippingMethodAction();
		  			$this->getOnepage()->saveShippingMethod($this->getRequest()->getPost('shipping_method', 'freeshipping_freeshipping'));
			  	} catch (Mage_Payment_Exception $e) {
					if ($e->getFields()) {
       	         		$result['fields'] = $e->getFields();
	            	}
       	     		$result['error'] = $e->getMessage();
	         	} catch (Mage_Core_Exception $e) {
	       	     	$result['error'] = $e->getMessage();
				} catch (Exception $e) {
					Mage::logException($e);
	            	$result['error'] = $this->__('Unable to set Payment Method.');
		  		}	
		  		if (!isset($result['error'])) {
		  			#$this->loadLayout('checkout_onepage_review');
		  			$result['goto_section'] = 'review';
	                $result['update_section'] = array(
              	    	'name' => 'review',
       	             	'html' => $this->_getReviewHtml()
	                );
		  		}
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }


	public function saveShippingMethodAction() {
		if ($this->_expireAjax()) {
        	return;
		}
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getPost('shipping_method', 'freeshipping_freeshipping');
			$result = $this->getOnepage()->saveShippingMethod($data);
			/*
	    	$result will have erro data if shipping method is empty
       		*/
        	if(!$result) {
        		Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request'=>$this->getRequest(), 'quote'=>$this->getOnepage()->getQuote()));
        		$result = $this->getOnepage()->saveShippingMethod($data);
        		#$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
					
            	$this->loadLayout('checkout_onepage_review');
            	$result['goto_section'] = 'review';
				$result['update_section'] = array(
					'name' => 'review',
                	'html' => $this->_getReviewHtml()
				);
            }
            
			if (!isset($result['error'])) {
				try {
					$resultPayment = $this->getOnepage()->savePayment(array('method'=>'checkmo'));
					if (empty($resultPayment['error'])) {
						$this->loadLayout('checkout_onepage_review');
						$resultPayment['goto_section'] = 'review';
						$resultPayment['update_section'] = array(
							'name' => 'review',
							'html' => $this->_getReviewHtml()
						);
					}
				} catch (Mage_Payment_Exception $e) {
            		if ($e->getFields()) {
						$resultPayment['fields'] = $e->getFields();
            		}
					$resultPayment['error'] = $e->getMessage();
				} catch (Mage_Core_Exception $e) {
					$resultPayment['error'] = $e->getMessage();
				} catch (Exception $e) {
					Mage::logException($e);
					$resultPayment['error'] = $this->__('Unable to set Payment Method.');
				}
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($resultPayment));
			} else {
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
			}
        }
	}
	
	public function saveOrderAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $this->saveShippingMethodAction();
        parent::saveOrderAction();
    }
}

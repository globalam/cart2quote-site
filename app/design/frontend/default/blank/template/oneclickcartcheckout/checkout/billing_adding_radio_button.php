
<fieldset>
  <ul class="form-list">
    <?php if ($this->customerHasAddresses()): ?>
    <li class="wide">
      <label for="billing-address-select"><?php echo $this->__('Select a billing address from your address book or enter a new address.') ?></label>
      <div class="input-box"> <?php echo $this->getAddressesHtmlSelect('billing') ?> </div>
    </li>
    <?php endif; ?>
    <li id="billing-new-address-form"<?php if ($this->customerHasAddresses()): ?> style="display:none;"<?php endif; ?>>
      <fieldset>
        <input type="hidden" name="billing[address_id]" value="<?php echo $this->getAddress()->getId() ?>" id="billing:address_id" />
        <ul>
          <li class="fields"><?php echo $this->getLayout()->createBlock('customer/widget_name')->setObject($this->getAddress()->getFirstname() ? $this->getAddress() : $this->getQuote()->getCustomer())->setFieldIdFormat('billing:%s')->setFieldNameFormat('billing[%s]')->toHtml() ?></li>
          <li class="fields">
            <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/company_status')!=1):?>
            <div class="field">
              <label for="billing:company" class="required"><em>*</em><?php echo $this->__('Company') ?></label>
              <div class="input-box">
                <input type="text" id="billing:company" name="billing[company]" value="<?php echo $this->htmlEscape($this->getAddress()->getCompany()) ?>" title="<?php echo $this->__('Company') ?>" class="input-text required-entry" />
              </div>
            </div>
            <?php endif;?>
            <?php if(!$this->isCustomerLoggedIn()): ?>
            <div class="field">
              <label for="billing:email" class="required"><em>*</em><?php echo $this->__('Email Address') ?> <span style="font-weight:900; color:#900;">(You will not receive a quote without an email address)</span></label>
              <div class="input-box">
              
              
                <input type="text" name="billing[email]" id="billing:email" value="<?php echo $this->htmlEscape($this->getAddress()->getEmail()) ?>" title="<?php echo $this->__('Email Address') ?>" style="<?php if(isset($_POST['submit_form']) && !isset($_POST['email'])){echo "background:#b12828;";} ?>" />
              </div>
            </div>
            <?php endif ?>
          </li>

              <input type="hidden" title="<?php echo $this->__('Street Address') ?>" name="billing[street][]" id="billing:street1" value="Address N/A for Quotes" class="input-text required-entry" />
              
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/second_address_line')!=1) { for ($_i=2, $_n=$this->helper('customer/address')->getStreetLines(); $_i<=$_n; $_i++): ?>
          <li class="wide">
            <div class="input-box">
              <input type="text" title="<?php echo $this->__('Street Address %s', $_i) ?>" name="billing[street][]" id="billing:street<?php echo $_i?>" value="<?php echo $this->htmlEscape($this->getAddress()->getStreet($_i)) ?>" class="input-text" />
            </div>
          </li>
          
          <?php endfor; } ?>
          <li class="fields">
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/city_status')!=1):?>
          <div class="field">
            <label for="billing:city" class="required"><em>*</em><?php echo $this->__('City') ?></label>
            <div class="input-box">
              <input type="text" title="<?php echo $this->__('City') ?>" name="billing[city]" value="<?php echo $this->htmlEscape($this->getAddress()->getCity()) ?>" class="input-text required-entry" id="billing:city" />
            </div>
          </div>
          <?php endif;?>
         
              <input type="hidden" id="billing:region" name="billing[region]" value="NH"  title="<?php echo $this->__('State/Province') ?>" class="input-text" style="display:none;" />
              
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/zip_status')!=1):?>
          <div class="field">
            <label for="billing:postcode" class="required"><em>*</em><?php echo $this->__('Zip/Postal Code') ?></label>
            <div class="input-box">
              <input type="text" title="<?php echo $this->__('Zip/Postal Code') ?>" name="billing[postcode]" id="billing:postcode" value="<?php echo $this->htmlEscape($this->getAddress()->getPostcode()) ?>" class="input-text validate-zip-international required-entry" />
            </div>
          </div>
          <?php endif;?>
          
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/country_status')=="detect" || !Mage::getStoreConfig('checkout/oneclickcartcheckout/country_status')):?>
          <div class="field">
            <label for="billing:country_id" class="required"><em>*</em><?php echo $this->__('Country') ?></label>
            <div class="input-box"> <?php echo $this->getCountryHtmlSelectDetect('billing') ?> </div>
          </div>
          
          <?php elseif (Mage::getStoreConfig('checkout/oneclickcartcheckout/country_status')=="enabled"):?>
          <div class="field">
            <label for="billing:country_id" class="required"><em>*</em><?php echo $this->__('Country') ?></label>
            <div class="input-box"> <?php echo $this->getCountryHtmlSelect('billing') ?> </div>
          </div>
          <?php endif;?>
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/phone_status')!=1):?>
          <div class="field">
            <label for="billing:telephone" class="required"><em>*</em><?php echo $this->__('Telephone') ?></label>
            <div class="input-box">
              <input type="text" name="billing[telephone]" value="<?php echo $this->htmlEscape($this->getAddress()->getTelephone()) ?>" title="<?php echo $this->__('Telephone') ?>" class="input-text required-entry" id="billing:telephone" />
            </div>
          </div>
          </li>
          <?php endif;?>
          <?php if(Mage::getStoreConfig('checkout/oneclickcartcheckout/fax_status')!=1):?>
          
           <li class="fields">
          <div class="field">
            <label for="billing:fax"><?php echo $this->__('Questions, Comments, Promo Codes') ?></label>
            <div class="input-box">
              <textarea name="billing[fax]" title="<?php echo $this->__('Questions, Comments, Promo Codes') ?>" class="input-text" id="billing:fax"></textarea>
            </div>
          </div>
          <div class="field">
          <div class="input-box">
          
          <input type="radio" name="group1" value="Milk"> Milk<br>
<input type="radio" name="group1" value="Butter" checked>


              <input type="radio" title="<?php echo $this->__('Would you prefer to be contacted via Email or Phone?') ?>" name="billing[street]" id="billing:street" value="Email" /><input type="radio" title="<?php echo $this->__('Would you prefer to be contacted via Email or Phone?') ?>" name="billing[street]" id="billing:street" value="Phone" />
            </div>
          </div>
          </li>
          <?php endif;?>

          
          <?php /* if ($this->isCustomerLoggedIn() && $this->customerHasAddresses()):?>
                    <li class="control">
                        <input type="checkbox" name="billing[save_in_address_book]" value="1" title="<?php echo $this->__('Save in address book') ?>" id="billing:save_in_address_book" onchange="if(window.shipping) shipping.setSameAsBilling(false);"<?php if ($this->getAddress()->getSaveInAddressBook()):?> checked="checked"<?php endif;?> class="checkbox" /><label for="billing:save_in_address_book"><?php echo $this->__('Save in address book') ?></label>
                    </li>
                <?php else:?>
                    <li class="no-display"><input type="hidden" name="billing[save_in_address_book]" value="1" /></li>
                <?php endif; */ ?>
        </ul>
      </fieldset>
    </li>
    <?php if ($this->canShip()): ?>
    <?php endif; ?>
  </ul>
  <?php if (!$this->canShip()): ?>
  <input type="hidden" name="billing[use_for_shipping]" value="1" />
  <?php endif; ?>
</fieldset>
<script type="text/javascript">
//<![CDATA[

		var updateCountryUrl = "<?php echo $this->getUrl('oneclickcartcheckout/checkout/updateCountry') ?>";

    var billing = new Billing('co-billing-form', '<?php echo $this->getUrl('checkout/onepage/getAddress') ?>address/', '<?php echo $this->getUrl('checkout/onepage/saveBilling') ?>');
    var billingForm = new VarienForm('co-billing-form');

    //billingForm.setElementsRelation('billing:country_id', 'billing:region', '<?php echo $this->getUrl('directory/json/childRegion') ?>', '<?php echo $this->__('Select State/Province...') ?>');
    $('billing-address-select') && billing.newAddress(!$('billing-address-select').value);

    var billingRegionUpdater = new RegionUpdater('billing:country_id', 'billing:region', 'billing:region_id', countryRegions, undefined, 'billing:postcode');

    function showPasswordFields(){
		$("register-customer-password").toggle();
        }
//]]>
</script>
</div>

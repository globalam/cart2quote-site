=== WordPress Leads ===

Contributors: David Wells, adbox
Donate link: mailto:marketplace@inboundnow.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: leads, lead capture, lead tracking, lead collection, lead management, crm, crm tools,customer relationship management, contact management, landing page leads, extendable 
Requires at least: 3.4
Tested up to: 3.6
Stable Tag: 1.0.7

Capture & store lead information, gather critical lead intelligence, manage web leads from within your site and close more deals.

== Description ==

Wordpress Leads is Wordpress's first fully extendable plugin for local CRM (Customer Relationship Management). Lead Management Plugin listens for form submissions & collects & stores visitor data into Wordpress as well as provides a record management interface for viewing and editing lead data. 

Learn where your leads are converting from geographically, what pages they viewed before filling out your form, quickly view any past website conversions or notes and use this information to sell more effectively.

Wordpress Leads was originally built as an add-on for WordPress Landing Pages but also can be used as a stand alone plugin to capture lead information on non wordpress landing pages.

Lead Management makes use of the Wordpress Core Custom Post Type API as well as stored funnel conversion data for each form a user submits his information through. Lead Management also makes use of geolocation services to automatically detect location details about a customer upon first conversion. Lead data can be managed, altered, and updated from a CPT management area. 

This plugin is built to be fully extendable by providing custom action and filter hooks, allowing for an almost endless number of powerful CRM application addons. 

= Highlights =

* Automatically detect and collect visitor data from any submitted form.
* Track and see what pages your leads visited before converting on your site to gain valuable lead intelligence
* Integrates seamlessly with WordPress Landing Pages
* Easily search, view, and modify lead (contact) information.
* Uses geolocation services to detect additional information on first time conversion.
* Easy to use CRM interface.
* Extend functionality with 3rd party extensions.
* Sync your leads with a third party CRM like salesforce.com, Zoho or sugarCRM.

This plugin is form agnostic meaning it will work with any almost any form system you use.

Recommended form plugins (Gravity forms, Ninja Forms or Contact Form 7)

Other recommended plugins (Wordpress Landing Pages)

= About the Plugin =

This is a free plugin that was built to help people collect, store, and manage lead/contact information to better understand each lead that comes into their site. It's built with inbound marketing best practices in mind and integrates seamlessly with the free WordPress Landing Pages plugin. 

= Developers & Designers = 

We built Lead Management as a framework! You can use our extendable framework to bring custom solutions to your application.

== Installation ==

1. Upload `wordpress-leads` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

1. Lead Management Custom Post Type
2. View and Edit Customer(Lead) Information
3. View Conversion Funnel Log for Each Customer(Lead)

== Changelog ==

= 1.0.7 =
* Updated lead tracking

= 1.0.0.5 =
* Fix issue with lead's first conversion not inserting into database correctly.

= 1.0.0.2 =
* Improved list view layout
* Fixed some JS errors

= 1.0.0.1 =

Released

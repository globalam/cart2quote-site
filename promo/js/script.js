/* Author: Ivor Padilla */

/* ---------- @ scrollTo -----------*/

 $(".scrolltoanchor").click(function() {
     $.scrollTo(
     	$($(this).attr("href")), {
         duration: 750
     });
     return false;
 });

/* ---------- @ Popover -----------*/

$("a[rel=popover]")
  .popover({
    offset: 10,
    placement: "top"
});

/* ---------- @ Tooltips -----------*/

$("a[rel=tooltip-top]").tooltip({
	placement: "top"
});

$("a[rel=tooltip-right]").tooltip({
	placement: "right"
});

$("a[rel=tooltip-bottom]").tooltip({
	placement: "bottom"
});

$("a[rel=tooltip-left]").tooltip({
	placement: "left"
});
/* ---------- @ SlidesJS -----------*/

$('#banner').slides({
	preload: true,
	generateNextPrev: true,
	autoHeight: true,
	effect: "slide"
	//play: 5000
});

$('#endorsement').slides({
	preload: true,
	generateNextPrev: false,
	autoHeight: true,
	effect: "fade",
	generatePagination: false,
	play: 5000
});

/* ---------- @ noSelect -----------*/

$(function(){
    $.extend($.fn.disableTextSelect = function() {
        return this.each(function(){
            if($.browser.mozilla){//Firefox
                $(this).css('MozUserSelect','none');
            }else if($.browser.msie){//IE
                $(this).bind('selectstart',function(){return false;});
            }else{//Opera, etc.
                $(this).mousedown(function(){return false;});
            }
        });
    });
    $('.noSelect').disableTextSelect();//No text selection on elements with a class of 'noSelect'
});

/* ---------- @ Goodies -----------*/

// wrap image with <span class="image-wrap"> for styling
	$('.dark-container img.cout').each(function() {
		var imgClass = $(this).attr('class');
		$(this).wrap('<span class="image-wrap ' + imgClass + '" style="width: auto; height: auto;"/>');
		$(this).removeAttr('class');
});

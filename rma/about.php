<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--W3C Verified XHTML/CSS - Marc Meledandri 06.18.2008 -->
<head>
	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
	<title>About RMA Database</title>
	<link rel="stylesheet" href="/rma/marcstyle.css" />
</head>
<body>
<table width="100%">
		<tr>
			<td align="left" style="background-color:#FFFFFF">
				<a href="http://validator.w3.org/check?uri=referer" target='_main'><img
				src="/rma/images/valid-xhtml10.png"
				alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
			</td>
			<td align="center" style="background-color:#FFFFFF">
				<big><strong>RMA Database Interface Website</strong></big></td>
			<td align="right" style="background-color:#FFFFFF">
				<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.globalamericaninc.com/rma/marcstyle.css" target='_main'><img
				src="/rma/images/valid-css2-blue.png"
				alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center" style="background-color:#FFFFFF">
				<i>&copy; 2008 Global American, Inc.</i>
				<br />
				<br />
				<br />
			</td>
		</tr>
		<tr>
			<td style="background-color:#FFFFFF">&nbsp;</td>
			<td style="background-color:#FFFFFF">
				<strong>Changelog</strong>
			</td>
		</tr>
		<tr>
			<td style="background-color:#FFFFFF">&nbsp;</td>
			<td align="left" style="background-color:#FFFFFF">
			
			<!--
			
			<i>Version 2.2</i>
				<br />
				<ul>
					<li>General
						<ul>
							<li></li>	
						</ul>
					</li>
					<br />
					<li>Interface
						<ul>
							<li>Added a new field to the RMA Status Summary that lists the relevant RMA#.</li>
						</ul>
						<br />
					<li>Functionality
						<ul>
							<li></li>
						</ul>
					</li>	
				</ul>
				<br />
				<br />
				
				-->
				
				<i>Version 2.1</i>
				<br />
				<ul>
					<li>General
						<ul>
							<li>Back-end database cleanup including all references to sales reports and debit memo details. Also modified field names in certain tables to be more descriptively named.</li>	
						</ul>
					</li>
					<br />
					<li>Interface
						<ul>
							<li>Replaced checkboxes with mutually exclusive radio buttons on New/Edit Failure Analysis form for replacement part options.</li>
							<li>Added fields to Technician Report and subsequent submittals, Edit Tech Report and View/Print Tech Report to enter customer reference numbers and additional GAI Barcodes associated with an RMA.</li>
						</ul>
						<br />
					<li>Functionality
						<ul>
							<li>Implemented new form where users can search for RMA Technician reports by entering a Customer Reference Number.</li>
						</ul>
					</li>	
				</ul>
				<br />
				<br />
				<i>Version 2.0</i>
				<br />
				<ul>
					<li>Interface
						<ul>
							<li>Modified the wording for the various status states for Received, Tech Report, Failure Analysis and Closed RMA's to make them more concise and meaningful. Also modified all past entries to reflect the updated wording.</li>
							<li>Removed links to Edit Sales Report and View Print Sales Report as well as any references to Sales Report.</li>
							<li>Removed Status selection dropdown from New/edit Failure Analysis and subsequent submission forms.</li>
							<li>Removed Edit Debit Memo Details. Most data on the form was redundant and can now be found on Vendor Shipping or Receiving Pages as well as Add/Edit Notes.</li>
							<li>Added several details from the Failure Analysis to the vendor Enter Shipping Details page.</li>
							<li>Added several details from the Failure Analysis and Shipping Details to the vendor Enter Receiving Details page.</li>
						</ul>
						<br />
					<li>Bugfixes
						<ul>
							<li>When displaying the Open Customer RMA Report, if a technician report has not been submitted for an RMA, the warranty information now defaults to 'Unknown' rather than repeatedly displaying the results of the most recent successful query.</li>
							<li>Corrected spelling of the sender address where automatic email is generated due to shipping or receiving details being entered for a customer RMA. Changed to rma@globalamericaninc.com from rma@globalamericanincan.com.</li>
							<li>Fixed alignment and text wrapping issues in the receiving details pane of View/Print Failure Analysis.</li>
							<li>Fixed issue where the 'Returning For' radio button options were not auto-populating in the Edit Shipping Details form.</li>	
						</ul>
					</li>
					<br />
					<li>Functionality
						<ul>
							<li>Designed and implemented a new report to display an RMA Status Summary. This report is displayed in a flow-chart format showing a color-coded chronological progression of dates relative to the various RMA process stages.</li>
							<li>Removed Sales Report functionality entirely as it was redundant and unnecessary. Cost data for Technician Time is now inputted and edited through the Technician Report interfaces.</li>
							<li>Vendor RMA Status changes are now hard-coded into the submission forms for Failure Analysis, Shipping and Receiving.</li>
							<li>Added a new tool to allow additional notes to be filed pertaining to Vendor RMA's. These notes will be displayed on the Open Vendor RMA Report as well as the shipping and receiving forms.</li>
							<li>Incoming customer requests are now screened against the database to determine if the part has been an RMA previously. If so, either a '-2' or '-3' will be automatically appended to the GAI Barcode Number to differentiate 2nd and 3rd-time RMA's respectively.</li>
							<li>Added the ability to edit existing Vendor Receiving Details.</li>
						</ul>
					</li>	
				</ul>
				<br />
				<br />
				<i>Version 1.2</i>
				<br />
				<ul>
					<li>Interface
						<ul>
							<li>Added Field to View/Print Failure Analysis to display the Customer RMA# associated with a Vendor RMA Failure Analysis.</li>
							<li>New Technician Report form now shows "Under Warranty" set to "No" as default setting.</li>
							<li>Changed Wording on Failure Analysis and resulting email to more accurately reflect when a part does not need to be purchased to replace a part for a customer.</li>
						</ul>
						<br />
					<li>Bugfixes
						<ul>
							<li>Fixed a bug where "issue noted" data on Technician Report was not being carried over on subsequent submittals.</li>
							<li>Fixed and issue causing the status of a Customer RMA to fail to update when a new Technician Report is submitted.</li>
							<li>Fixed issue where the technician edits an incorrect bar code number, originally supplied by the customer request, in the Edit RMA (Technician) form, the corresponding sales form becomes irretrievable.</li>
						</ul>
					</li>
					<br />
					<li>Functionality
						<ul>
							<li>When Shipping/Receiving information is entered regarding a Customer RMA, email options are offered. Selecting "Don't Send Email" now overrides any other options selected.</li>
						</ul>
					</li>	
				</ul>
				<br />
				<br />			
				<i>Version 1.1</i>
				<br />
				<ul>
					<li>Interface
						<ul>
							<li>Removed checkboxes for "Under Warranty" from New Technician Report, Edit RMA (Technician) and Edit RMA (Sales). Replaced checkboxes with radio buttons which are now mutually exclusive.</li>
							<li>Removed checkboxes from View/Print Technician Report. Replaced with Text Only</li>
							<li>Removed Quantity from New/Edit Failure Analysis as it always has a value equal to 1.</li>
							<li>Removed Vendor from New/Edit Failure Analysis as Technician would not have that information available.</li>
							<li>Removed Quantity and Tech Entered RMA from View/Print Failure Analysis as neither is required.</li>
							<li>Added more detailed information to automatically generated email when a Vendor Failure Analysis is completed.</li>
						</ul>
						<br />
					<li>Bugfixes
						<ul>
							<li>Fixed a bug in Customer Open RMA Report where overly complex query would not return new customer requests until a technician report had been completed. Use of a nested query of the multiple tables simplifies this and fixes the issue.</li>
							<li>Fixed a spelling error in Vendor RMA Receiving Information form.</li>
							<li>Fixed an issue that was causing leading whitespace to appear before the details in Test Configuration text area in New/Edit Failure Analysis.</li>
							<li>Fixed a bug in Customer RMA Shipping Edit where newly created RMA's are not listed. All new customer requests are now listed and the sort order is by RMA#.</li>
							<li>Fixed a bug where "Issue Noted By..." on New/Edit Failure Analysis was not writing user input to the database.</li>
							<li>Fixed bug involving Edit Shipping Details dropdown list selection leading to 'report not found for barcode#:' error on some RMA's.</li>
						</ul>
						<br />
					</li>
					<li>Funtionality
						<ul>
							<li>Added display of original invoice date to Open Customer RMA Report.</li>
							<li>Removed the option to select 'Unknown' in response to 'Part Needed For Customer Order?' in New/Edit Failure Analysis and subsequent submissions.</li>
							<li>Added new input table field for explanation if technician replies 'No' to above question. This is to be completed in the New/Edit Failure Analysis and subsequent submissions and is displayed when viewing, editing or printing previous Analyses.</li>
						</ul>
					</li>					
				</ul>
				<br />
				<br />
				<i>Version 1.0</i>
				<br />
				<ul>
					<li>General
					<ul>
						<li>Rewrote entire website code to comply with World Wide Web Consortium (W3C) XHTML 1.0 specification.</li>
						<li>Replicated most naming conventions and form layout such that users of the former database application will be immediately familiar with the various tools.</li>
						<li>Added a comprehesive set of Administrative Tools to perform various database maintenance tasks including adding and removing users, managing passwords, and cleanup of incorrect or unnecessary data in the database.</li>
						<li>Provided extensive in-code documentation enabling future database administrators to be easily able to extend and modify the existing code base.</li>
					</ul>
					<br />
					</li>
					<li>Interface
					<ul>
						<li>Implemented the use of Cascading Style Sheet (CSS) for consistant look and feel throughout the site.</li>
						<li>Removed unnecessary JavaScript code throughout the site to provide a cleaner, faster and more intuitive user interface.</li>
					</ul>
					<br />
					</li>
					<li>Bugfixes
					<ul>
						<li>Fixed issue with database function call in Edit Vendor RMA not stripping invalid Debit Memo numbers.</li>
						<li>Fixed issue in drop-down list where Edit Existing Analysis displayed invalid and unordered barcode numbers.</li>
						<li>Fixed bug where requesting RMA lookup by company only returned the first result.</li>
						<li>Fixed various spelling and grammatical errors throughout site including the standardized emails sent to customers.</li>
					</ul>
					<br />
					</li>
					<li>Funtionality
					<ul>
						<li>Added display of current barcode being modified in both the Vendor RMA Shipping Information Edit and the RMA Receiving Information Edit forms.</li>
						<li>RMA Status is now set to Closed if 'Credit' or 'Scrap' are selected when editing Vendor RMA Shipping Information.</li>
						<li>Added the ability to sort information contained in both the Vendor and Customer RMA Reports by their relevant column headers.</li>
						<li>Users and their respective passwords are now database-driven rather than being simple form variables.</li>
						<li>User administration tools have been created to add, remove and modify user data</li>
						<li>Administration tools created to deal with incorrect data in the database (e.g. RMA pulled twice by accident, RMA not closed when it should be, etc.).</li>
						<li>When viewing Open Customer RMA reports, the GAI Barcode number now displays as a link. Following this link brings you to the relevant Technician Report for the RMA.</li>
						<li>When viewing any of the Vendor RMA reports, the GAI Barcode number now displays as a link. Following this link brings you to the relevant Failure Analysis Report for the RMA.</li>
						<li>Modified Customer Open RMA report per user request: Removed Invoice #, Inv. Date, PO #, and PO Date. Added Warranty Status and Warranty Expiration Date.</li>
						<li>Users now have the option to download to their local machines the various reports available in Microsoft Excel (.xls) file format.</li>
					</ul>
					</li>
				</ul>
			</td>
		</tr>
	</table>
</body>
</html>
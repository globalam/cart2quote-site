<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 01.24.2008 -->
<head>
	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
	<title>Global American RMA Login</title>
	<link rel="stylesheet" href="/rma/marcstyle.css" />
</head>
<body style="background-color: #990000">
	<form action="/rma/admin_toolset.php" method="post">
		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>
			<tr>
				<th colspan="2"><span class="head-text">RMA Database Administration Tools</span> </th>
			</tr>
			<tr>
				<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align='center' colspan="2">					
					<span class="title-text">Select from the following tools: </span>		
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td width="300" align="right">Create New User:</td>
				<td align="left"><input type="radio" name="tool" value="new_user" checked="checked" /></td>
			</tr>
			<tr>
				<td align="right">Remove Existing User:</td>
				<td align="left"><input type="radio" name="tool" value="remove_user" /></td>
			</tr>
			<tr>
				<td align="right">Change User Password:</td>
				<td align="left"><input type="radio" name="tool" value="change_pwd" /></td>
			</tr>
			<tr>
				<td align="right">List All Users:</td>
				<td align="left"><input type="radio" name="tool" value="list_users" /></td>
			</tr>
			<tr>
				<td align="right">Change Vendor RMA Status:</td>
				<td align="left">
					<input type="radio" name="tool" value="change_vendstatus" /></td>
			</tr>
			<tr>
				<td align="right">Change Customer RMA Status:</td>
				<td align="left">
					<input type="radio" name="tool" value="change_custstatus" /></td>
			</tr>
			<tr>
				<td align="right">Remove a Vendor RMA:</td>
				<td align="left"><input type="radio" name="tool" value="remove_vendrma" /></td>
			</tr>
			<tr>
				<td align="right">Remove a Customer RMA:</td>
				<td align="left"><input type="radio" name="tool" value="remove_custrma" /></td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input class="form-button" type="submit" name="Submit" value="Submit" />
				</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
		</table>
	</form>
</body>
</html>
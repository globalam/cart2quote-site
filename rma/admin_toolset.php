<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.24.2008 -->

	<head>

		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

		<title>Admin Toolset</title>

		<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

	</head>

<body style="background-color: #990000">

<?php

$tool=$_POST['tool']; 

if ($tool == new_user){

	echo"

		<form id='new_user' method='post' action='admin_new_user.php'>

			<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

				<tr>

					<th colspan='2'><span class='head-text'>Add New User</span></th>

				</tr>

				<tr>

					<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td align='right'>Enter GAI Domain User Name:</td>

					<td>

						<input name='newusername' type='text' id='newusername' />

					</td>

				</tr>

				<tr>

					<td align='right'>Enter password for new user:</td>

					<td>

						<input name='newuserpass' type='text' id='newuserpass' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

				<tr>

					<td colspan='2' align='center'>

						<input class='form-button' type='submit' name='Submit' value='Submit' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

			</table>

		</form>

	";//end echo

}//end new_user

if ($tool == remove_user){

	echo"

		<form id='remove_user' method='post' action='admin_remove_user.php'>

			<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

				<tr>

					<th colspan='2'><span class='head-text'>Remove User</span></th>

				</tr>

				<tr>

					<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td align='right'>Enter GAI Domain User Name:</td>

					<td>

						<input name='removeuser' type='text' id='removeuser' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

				<tr>

					<td colspan='2' align='center'>

						<input class='form-button' type='submit' name='Submit' value='Submit' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

			</table>

		</form>

	";//end echo

}//end remove_user

if ($tool == change_pwd){

	echo"

		<form id='change_pwd' method='post' action='admin_change_pwd.php'>

			<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

				<tr>

					<th colspan='2'><span class='head-text'>Change User Password</span></th>

				</tr>

				<tr>

					<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td align='right'>Enter GAI Domain User Name:</td>

					<td>

						<input name='username' type='text' id='username' />

					</td>

				</tr>

				<tr>

					<td align='right'>Enter NEW password for user:</td>

					<td>

						<input name='change' type='text' id='change' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

				<tr>

					<td colspan='2' align='center'>

						<input class='form-button' type='submit' name='Submit' value='Submit' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

			</table>

		</form>

	";//end echo

}//end change_pwd

if ($tool == list_users){

	echo"

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan='2'><span class='head-text'>RMA Database Website Users</span></th>

			</tr>

			<tr>

				<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

			</tr>

			<tr>

				<td colspan='2'>&nbsp;</td>

			</tr>

			<tr>

				<td align='center'><strong>User Name</strong></td>

				<td align='center'><strong>Password</strong></td>

			</tr>";	

			include("/home/globalam/public_html/includes/configure.php") ;

			$connection = mysql_connect ("", "$user", "$password");

			if ($connection == false){

				echo mysql_errno().": ".mysql_error()."<br />";

				exit;

			}//end if

			$query = "SELECT * FROM members WHERE username != 'Admin' ORDER BY username";

			$result = mysql_db_query ("globalam_magento", $query);

			if ($result){

				$numOfRows = mysql_num_rows ($result);

				for ($i = 0; $i < $numOfRows; $i++){

					$username = mysql_result ($result, $i, "username");

					$userpass = mysql_result ($result, $i, "password");

					echo"

						<tr>

							<td align='center'>$username</td>

							<td align='center'>$userpass</td>

						</tr>					

					";//end echo

				}//end for

			}//end if

			else{

				echo"There was a problem";

				}//end else

			echo"

			<tr>

				<td colspan='4'>&nbsp;</td>

			</tr>

		</table>

	";//end echo

}//end list_users

if ($tool == change_vendstatus){

	echo"

		<form id='vendstat' method='post' action='admin_change_vendstat.php'>

			<table width='600' frame='box' rules='none' rules='none' cellpadding='2' cellspacing='0'>

				<tr>

					<th colspan='2'><span class='head-text'>Change Vendor RMA Status</span></th>

				</tr>

				<tr>

					<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td align='right'>Enter Vendor Name:</td>

					<td>

						<input name='vend' type='text' id='vend' />

					</td>

				</tr>

				<tr>

					<td align='right'>Enter GAIj Part Number:</td>

					<td>

						<input name='sn' type='text' id='sn' />

				</td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td colspan='2' align='center'>

						<input class='form-button' type='submit' name='Submit' value='Submit' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

			</table>

		</form>

	";//end echo

}//end change_vendstatus

if ($tool == change_custstatus){

echo"change_custstatus";

}//end change_custstatus

if ($tool == remove_vendrma){

echo"remove_vendrma";

}//end remove_vendrma

if ($tool == remove_custrma){

	echo"

		<form id='remove_cust' method='post' action='admin_remove_cust.php'>

			<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

				<tr>

					<th colspan='2'><span class='head-text'>Remove Customer Request</span></th>

				</tr>

				<tr>

					<td colspan='2' class='small-red' align='left'><span >This section for Database Administrator ONLY!</span></td>

				</tr>

				<tr>

					<td colspan='2'>&nbsp;</td>

				</tr>

				<tr>

					<td align='right'>Enter RMA Number:</td>

					<td>

						<input name='removeRma' type='text' id='removeRma' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

				<tr>

					<td colspan='2' align='center'>

						<input class='form-button' type='submit' name='Submit' value='Submit' />

					</td>

				</tr>

				<tr>

					<td colspan='4'>&nbsp;</td>

				</tr>

			</table>

		</form>

	";//end echo

}//end remove_custrma

?>

</body>

</html>


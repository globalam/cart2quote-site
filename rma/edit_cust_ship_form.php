<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.16.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Edit Shipping Details</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?php

	include("/home/globalam/public_html/includes/configure.php") ;

	$sn=$_POST['sn_bc'];

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

		echo mysql_errno().": ".mysql_error()."<br />";

		exit;

	}// End if

	$query = "select * from requests where Serial_Number='$sn' ";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$id = mysql_result ($result, $i, "id");

			$company = mysql_result ($result, $i, "Company");

			$Part_Number = mysql_result ($result, $i, "Part_Number");

			$Quantity = mysql_result ($result, $i, "Quantity");

			$date = mysql_result ($result, $i, "date");

			$rec_date = mysql_result ($result, $i, "rec_date");

			$ship_date = mysql_result ($result, $i, "ship_date");

		}//end for

	}//end if

	else{

	echo "Trouble connecting to the database, try again later.<br />";

	echo mysql_errno().": ".mysql_error()."<br />";

	}

	?>



	<form method="post" action="submit_cust_ship_edit.php">

	<input type="hidden" name="sn" value="<?php echo "$sn" ; ?>" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan='2'><span class="head-text">Edit RMA Shipping Details</span></th>

			</tr>

			<tr>

				<td colspan='2' class='small-red' align='left'><span >This section to be completed by Shipping</span></td>

			</tr>

			<tr>

				<td colspan="2">&nbsp;</td>

			</tr>

			<tr> 

				<td><strong>&nbsp;&nbsp;&nbsp;RMA Number:</strong></td>

				<td><strong><?php echo "$id" ; ?></strong></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Customer:</td>

				<td><?php echo "$company" ; ?></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Part Number:</td>

				<td><?php echo "$Part_Number" ; ?></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Quantity:</td>

				<td><?php echo "$Quantity" ; ?></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Serial#:</td>

				<td><?php echo "$sn" ; ?></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Date Issued:</td>

				<td><?php echo "$date" ; ?></td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Date Received:</td>

				<td> 

					<input type="text" name="rec_date" value="<?php echo "$rec_date" ; ?>" />

				</td>

			</tr>

			<tr> 

				<td>&nbsp;&nbsp;&nbsp;Date Shipped:</td>

				<td> 

					<input type="text" name="ship_date"  value="<?php echo "$ship_date" ; ?>" />

				</td>

			</tr>

			<tr>

				<td colspan='4'>&nbsp;</td>

			</tr>

			<tr>

				<td colspan='4' align='center'>

					<input class='form-button' type='submit' value="Submit" name="Submit" />

				</td>

			</tr>

			<tr>

				<td colspan='4'>&nbsp;</td>

			</tr>

		</table>

	</form>

</body>

</html>
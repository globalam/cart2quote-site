<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 06.18.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Edit Existing RMA</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?php

	$rma = $_POST['rma'];

	include("/home/globalam/public_html/includes/configure.php") ;

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

		echo mysql_errno().": ".mysql_error()."<br />";

		exit;

	}//end if

$query = "SELECT * FROM requests WHERE id ='$rma'";

$result = mysql_db_query ("globalam_magento", $query);

if ($result){

	$numOfRows = mysql_num_rows ($result);

	for ($i = 0; $i < $numOfRows; $i++){

		$id = mysql_result ($result, $i, "id");
		$company = mysql_result ($result, $i, "Company");
		$contact = mysql_result ($result, $i, "Contact");
		$st = mysql_result ($result, $i, "Street");
		$City = mysql_result ($result, $i, "City");
		$State = mysql_result ($result, $i, "State");
		$Zip = mysql_result ($result, $i, "Zip");
		$Country = mysql_result ($result, $i, "Country");
		$Phone = mysql_result ($result, $i, "Phone");
		$Email = mysql_result ($result, $i, "Email");
		$Part_Number = mysql_result ($result, $i, "Part_Number");
		$Quantity = mysql_result ($result, $i, "Quantity");
		$Serial_Number = mysql_result ($result, $i, "Serial_Number");
		$Invoice = mysql_result ($result, $i, "Invoice");
		$Invoice_Date = mysql_result ($result, $i, "Invoice_Date");
		$PO_Number = mysql_result ($result, $i, "PO_Number");
		$PO_Date = mysql_result ($result, $i, "PO_Date");
		$Problem = htmlentities(mysql_result ($result, $i, "Problem"));

		$Emailed = mysql_result ($result, $i, "Emailed");
		$date = mysql_result ($result, $i, "date");
		$rec_date = mysql_result ($result, $i, "rec_date");
		$ship_date = mysql_result ($result, $i, "ship_date");
		$approved = mysql_result ($result, $i, "approved");

	}//end for

}//end if

else{

	echo "Trouble1 connecting to the database, try again later.";

}//end else

	?>

	<form action="submit_existing_rma_edit.php" method="post" enctype="multipart/form-data">

	<input name="fmchk" type="hidden" value="true" />

	<input name="rma" type="hidden" value="<?php echo"$rma"; ?>" />

	<input name="id" type="hidden" value="<?php echo"$id"; ?>" />

	<input name="cost" type="hidden" value="<?php echo"$cost"; ?>" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="4"><span class="head-text">Edit Existing Customer RMA</span></th>

			</tr>

			<tr>                                                                                       

				<td colspan='4' class='small-red' align='left'><span >This section to be completed by someone compitent </span></td>

			</tr>
            
            <tr>
			  <td><strong>RMA Status:<br /><?php echo $approved ; ?></strong></td>  
  
			  <td style="background: #FF0;"><input name="approved" type="radio" id="approved_requested" <?php if($approved <= 1 ){echo 'checked';}?> value="1" />Requested</td>
			  <td style="background: #0F0;"><input name="approved" type="radio" id="approved_open" <?php if($approved == 2){echo 'checked';}?> value="2"/>Open(Approved)</td>
			  <td style="background: #F00;"><input name="approved" type="radio" id="approved_closed" <?php if($approved == 3 ){echo'checked';}?> value="3"/>Closed</td>
		  </tr>
			<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
		  </tr>
			<tr>

			<tr>

				<td><strong>RMA#:</strong></td>

				<td><strong><?php echo"$rma"; ?></strong></td>

				<td>&nbsp;</td>

				<td>

					&nbsp;

				</td>

			</tr>    

			<tr>
				<td><strong>Invoice:</strong></td>
				<td>
					<input name="invoice" type="text" id="invoice" size="10" maxlength="10" value="<?php echo"$Invoice"; ?>" />
				</td>
				<td><strong>Invoice Date:</strong></td>
				<td>
					<input name="invoice_date" type="text" id="invoice_date" size="10" maxlength="10" value="<?php echo"$Invoice_Date"; ?>" />
				</td>
			</tr>

            <tr>
				<td><strong>PO #:</strong></td>
				<td>
					<input name="po_number" type="text" id="po_number" size="10" maxlength="10" value="<?php echo"$PO_Number"; ?>" />
				</td>
				<td><strong>PO Date:</strong></td>
				<td>
					<input name="po_date" type="text" id="po_date" size="10" maxlength="10" value="<?php echo"$PO_Date"; ?>" />
				</td>
			</tr>

			<tr>
				<td><strong>Company:</strong></td>
				<td>
					<input name="company" type="text" id="company" size="25" maxlength="90" value="<?php echo"$company"; ?>" />
				</td>
				<td><strong>Contact:</strong></td>
				<td>
					<input name="contact" type="text" id="contact" size="25" maxlength="250" value="<?php echo"$contact"; ?>" />
				</td>
			</tr>
            
            <tr>
				<td><strong>P/N:</strong></td>
				<td>
					<input name="part_number" type="text" id="part_number" size="26" maxlength="70" value="<?php echo"$Part_Number"; ?>" />
				</td>
				<td><strong>S/N:</strong></td>
				<td>
					<input name="serial_number" type="text" id="serial_number" size="26" maxlength="70" value="<?php echo"$Serial_Number"; ?>" />
				</td>
			</tr>
            
            <tr>
				<td><strong>Problem:</strong></td>
				<td colspan="3">
                    <textarea name = "problem" cols=50  rows=4 ><?php echo $Problem; ?></textarea>
				</td>
			</tr>
            
			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td align="center" colspan="4">

					<input class="form-button" type="submit" name="Submit" value="Submit" />

					<br />&nbsp;

				</td>

			</tr>
            


		</table>

	</form>

</body>

</html>
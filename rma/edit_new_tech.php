<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 06.18.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>New Technician Customer RMA </title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?php

		$gairma = $_POST['gairma'];

		include("/home/globalam/public_html/includes/configure.php") ;

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

			echo mysql_errno().": ".mysql_error()."<br />";

			exit;

		}//end if

		$query = "SELECT Company , Part_Number, Serial_Number, Problem, approved FROM requests where id = '$gairma' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

			$cust = mysql_result ($result, $i, "Company");

			$pn = mysql_result ($result, $i, "Part_Number");

			$sn = mysql_result ($result, $i, "Serial_Number");

			$problem = mysql_result ($result, $i, "Problem");
			
			$approved = mysql_result ($result, $i, "approved");

			}//end for

		}//end if

		else{  echo "Error processing your request, please try again later."; exit; }

	?>

	<form action="submit_new_tech.php" method="post" enctype="multipart/form-data">

		<input name="fmchk" type="hidden" value="true" />

		<input name="rma" type="hidden" value="<?php echo"$gairma"; ?>" />

		<input name="cost" type="hidden" value="" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="4"><span class="head-text">Technician Report (Customer RMA)</span></th>

			</tr>

			<tr>

				<td colspan='4' class='small-red' align='left'><span >This section to be completed by Engineering </span></td>

			</tr>

			 <tr>
			  <td><strong>RMA Status:</strong></td>  
  
			  <td style="background: #FF0;"><input name="approved" type="radio" id="approved_requested" <?php if($approved <= 1 ){echo 'checked';}?> value="1" />Requested</td>
			  <td style="background: #0F0;"><input name="approved" type="radio" id="approved_open" <?php if($approved == 2){echo 'checked';}?> value="2"/>Open(Approved)</td>
			  <td style="background: #F00;"><input name="approved" type="radio" id="approved_closed" <?php if($approved == 3 ){echo'checked';}?> value="3"/>Closed</td>
		  </tr>
			<tr>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
		  </tr>
			<tr>

				<td><strong>RMA#:</strong></td>

				<td><strong><?php echo"$gairma"; ?></strong></td>

				<td><strong>GAI Barcode#:</strong></td>

				<td>

					<input name="sn_bc" type="text" id="sn_bc" size="15" maxlength="15" />

				</td>

			</tr>

			<tr>

				<td><strong>Date:</strong></td>

				<td>

					<input name="date" type="text" id="date" size="10" maxlength="10" />

				</td>

				<td><strong>Technician:</strong></td>

				<td>

					<input name="tech" type="text" id="tech" size="25" maxlength="250" />

				</td>

			</tr>

			<tr>

				<td><strong>Customer:</strong></td>

				<td>

					<input name="cust" type="text" id="cust" size="25" maxlength="30" value="<?php echo"$cust"; ?>" />

				</td>

				<td><strong>Cust Ref #:</strong></td>

				<td>

					<input name="custRefNum" type="text" id="custRefNum" size="25" maxlength="250" />

				</td>

			</tr>

			<tr>

				<td colspan="2"><strong>Under Warranty:</strong>

					<input name="warranty" type="radio" id="warranty_yes" value="Yes" />

					Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

					<input name="warranty" type="radio" id="warranty_no" value="No" checked="checked"/>

					No

				</td>

				<td><strong>Warranty Expires:</strong></td>

				<td>

					<input name="war_ex_date" type="text" id="war_ex_date" size="15" maxlength="250" />

				</td>

			</tr>

			<tr>

				<td colspan="4"><strong>Reported Defective Item: </strong><?php echo"$pn"; ?></td>

			</tr>

			<tr>

				<td colspan="4">

					<hr size='1' width='100%'/>

				</td>

			</tr>

			<tr>

				<td colspan="4"><strong>Accessories Included:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="accessories" cols="75" rows="3" id="accessories"></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Missing Accessories:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="missing" cols="75" rows="3" id="missing"></textarea></td>

			</tr>

			<tr>

				<td height="23" colspan="4"><strong>Damage:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="damage" cols="75" rows="3" id="damage"></textarea></td>

			</tr>

			<tr>

				<td height="23" colspan="4"><strong>Additional GAI Barcodes:</strong><small><i> (if applicable)</i></small></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="addSerials" cols="75" rows="3" id="addSerials"></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Issues Reported by Customer:</strong></td>

			</tr>

			<tr>

				<td colspan="4" class='small-red'><em><?php echo"$problem"; ?></em></td>

			</tr>

			<tr>

				<td colspan="4">

					<hr size='1' width='100%'/>

				</td>

			</tr>

			<tr>

				<td colspan="4"><strong>Issues Noted by Global American Technician:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="problem" cols="75" rows="4" id="problem"></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Resolution:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="resolution" cols="75" rows="15" id="resolution"></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Suggested Corrective Action:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="proposed" cols="75" rows="3" id="proposed"></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Associated Costs:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="charges" cols="75" rows="3" id="charges"></textarea></td>

			</tr>

			<tr>

				<td>Technician Time:</td>

				<td colspan="3"><input name="tech_time" type="text" id="tech_time" size="25" maxlength="30"  /></td>

			</tr>

			<tr>

				<td>Verfied By:</td>

				<td>

					<input name="verify" type="text" id="verify" size="25" maxlength="30" />

				</td>

				<td>Date:</td>

				<td>

					<input name="verify_date" type="text" id="verify_date" size="25" maxlength="30" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td align="center" colspan="4">

					<input class="form-button" type="submit" name="Submit" value="Submit" />

					<br />&nbsp;

				</td>

			</tr>

		</table>

	</form>

</body>

</html>
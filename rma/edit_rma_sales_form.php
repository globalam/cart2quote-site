<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.14.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Edit RMA Sales</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?PHP

	$sn_bc = $_POST['sn_bc'];

	include("/home/globalam/public_html/includes/configure.php") ;

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

		echo mysql_errno().": ".mysql_error()."<br>";

		exit;

	}//end if

	// Pull form data

	$query = "SELECT * FROM tech_report WHERE sn_bc = '$sn_bc' ";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$id = mysql_result ($result, $i, "id");

			$date = mysql_result ($result, $i, "date");

			$rma = mysql_result ($result, $i, "rma");

			$sn_bc = mysql_result ($result, $i, "sn_bc");

			$cust = mysql_result ($result, $i, "cust");

			$tech = mysql_result ($result, $i, "tech");

			$warranty = mysql_result ($result, $i, "warranty");

			$war_ex_date = mysql_result ($result, $i, "war_ex_date");

			$accessories = mysql_result ($result, $i, "accessories");

			$missing = mysql_result ($result, $i, "missing");

			$damage = mysql_result ($result, $i, "damage");

			$problem = mysql_result ($result, $i, "problem");

			$resolution = mysql_result ($result, $i, "resolution");

			$proposed = mysql_result ($result, $i, "proposed");

			$pics = mysql_result ($result, $i, "pics");

			$tech_time = mysql_result ($result, $i, "tech_time");

			$cost = mysql_result ($result, $i, "cost");

			$verify = mysql_result ($result, $i, "verify");

			$verify_date = mysql_result ($result, $i, "verify_date");

		}//end for

	}//end if

	else{echo "Error processing your request, please try again later.";}

	//RMA Request Database pull

	$query = "SELECT Company , Part_Number, Serial_Number, Problem FROM requests where id = '$rma' ";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

	  $numOfRows = mysql_num_rows ($result);

	  for ($i = 0; $i < $numOfRows; $i++){

	   

		$pn = mysql_result ($result, $i, "Part_Number");

		$sn = mysql_result ($result, $i, "Serial_Number");

		$reported_problem = mysql_result ($result, $i, "Problem");

	  }

	}

	else{  echo "Error processing your request, please try again later."; }

	?>	



<form action="submit_rma_sales_edit.php" method="post" enctype="multipart/form-data">

<input name="fmchk" type="hidden" value="true" />

<input name="rma" type="hidden" value="<?PHP echo"$rma"; ?>" />

<input name="id" type="hidden" value="<?PHP echo"$id"; ?>" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="4"><span class="head-text">Edit Customer RMA</span></th>

			</tr>

			<tr>

				<td colspan='4' class='small-red' align='left'><span >This section to be completed by Sales</span></td>

			</tr>	

			<tr>

				<td><strong>RMA#:</strong></td>

				<td colspan="3"><strong><?PHP echo"$rma"; ?></strong></td>

			</tr>

			<tr>

				<td><strong>Date:</strong></td>

				<td>

					<input name="date" type="text" id="date" size="10" maxlength="10" value="<?PHP echo"$date"; ?>" />

				</td>

				<td><strong>Serial# / Barcode#:</strong></td>

				<td>

					<input name="sn_bc" type="text" id="sn_bc" size="15" maxlength="15" value="<?PHP echo"$sn_bc"; ?>" />

				</td>

			</tr>

			<tr>

				<td><strong>Customer:</strong></td>

				<td>

					<input name="cust" type="text" id="cust" size="25" maxlength="30" value="<?PHP echo"$cust"; ?>" />

				</td>

				<td><strong>Technician:</strong></td>

				<td>

					<input name="tech" type="text" id="tech" size="25" maxlength="250" value="<?PHP echo"$tech"; ?>" />

				</td>

			</tr>

			<tr>

				<td colspan="2"><strong>Under Warranty:</strong>

					<input name="warranty_yes" type="checkbox" id="warranty_yes" value="Yes" <?PHP if($warranty == "Yes"){echo "checked";} ?> />

					Yes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

					<input name="warranty_no" type="checkbox" id="warranty_no" value="No" <?PHP if($warranty == "No"){echo "checked";} ?> />

					No

				</td>

				<td><strong>Warranty Expires:</strong></td>

				<td>

					<input name="war_ex_date" type="text" id="war_ex_date" size="15" maxlength="250" value="<?PHP echo"$war_ex_date"; ?>" /></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Reported Defective Item: </strong><?PHP echo"$pn"; ?></td>

			</tr>

			<tr>

				<td colspan="4">

					<hr size='1' width='100%'/>

				</td>

			</tr>

			<tr>

				<td colspan="4"><strong>Accessories Included:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="accessories" cols="75" rows="3" id="accessories"><?PHP echo"$accessories"; ?></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Missing Accessories:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="missing" cols="75" rows="3" id="missing"><?PHP echo"$missing"; ?></textarea></td>

			</tr>

			<tr>

				<td height="23" colspan="4"><strong>Damage:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="damage" cols="75" rows="3" id="damage"><?PHP echo"$damage"; ?></textarea></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Issues Reported by Customer:</strong></td>

			</tr>

			<tr>

				<td colspan="4" class='small-red'><em><?PHP echo"$reported_problem"; ?></em></td>

			</tr>

			<tr>

				<td colspan="4">

					<hr size='1' width='100%'/>

				</td>

			</tr>

			<tr>

				<td colspan="4"><strong>Issues Noted by Global American Technician:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="problem" cols="75" rows="4" id="problem"><?PHP echo"$problem"; ?></textarea></td>

			</tr>



			<tr>

				<td colspan="4"><strong>Resolution:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="resolution" cols="75" rows="15" id="resolution"><?PHP echo"$resolution"; ?></textarea></td>

			</tr>

			<tr>

				<td colspan="4" ><span class="style6"></span></td>

			</tr>

			<tr>

				<td colspan="4"><strong>Suggested Corrective Action:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="proposed" cols="75" rows="3" id="proposed"><?PHP echo"$proposed"; ?></textarea></td>

			</tr>



			<tr>

				<td colspan="4"><strong>Associated Costs:</strong></td>

			</tr>

			<tr>

				<td colspan="4"><textarea name="pics" cols="75" rows="3" id="pics"><?PHP echo"$pics"; ?></textarea></td>

			</tr>

			<tr>

				<td>Technician Time:</td>

				<td><input name="tech_time" type="text" id="tech_time" size="25" maxlength="30" value="<?PHP echo"$tech_time"; ?>" /></td>

				<td>Total Cost:</td>

				<td><input name="cost" type="text" id="cost" size="25" maxlength="30" value="<?PHP echo"$cost"; ?>" /></td>

			</tr>

			<tr>

				<td>Verfied By:</td>

				<td>

					<input name="verify" type="text" id="verify" size="25" maxlength="30" value="<?PHP echo"$verify"; ?>" />

				</td>

				<td>Date:</td>

				<td>

					<input name="verify_date" type="text" id="verify_date" size="25" maxlength="30" value="<?PHP echo"$verify_date"; ?>" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td align="center" colspan="4">

					<input class="form-button" type="submit" name="Submit" value="Submit" />

					<br>&nbsp;

				</td>

			</tr>

		</table>

	</form>

</body>

</html>
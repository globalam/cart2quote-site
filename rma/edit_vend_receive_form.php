<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 05.16.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Vendor RMA Receiving Details Editing</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?php

		$gaibc = $_POST['gaibc'];

		include("/home/globalam/public_html/includes/configure.php") ;

		// Connect to Database

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

		  echo mysql_errno().": ".mysql_error()."<br />";

		  exit;

		}// End If

		//Get barcode number for item currently editing

		$query = "SELECT * FROM eval_disp_eng WHERE gaibc = '$gaibc' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$eng_id = mysql_result ($result, $i, "id");

				$qty = mysql_result ($result, $i, "qty");

				$mfr = mysql_result ($result, $i, "mfr");

				$gaipn = mysql_result ($result, $i, "gaipn");

				$model = mysql_result ($result, $i, "model");

				$gaibc = mysql_result ($result, $i, "gaibc");

				$mfr_sn = mysql_result ($result, $i, "mfr_sn");

				$defect = mysql_result ($result, $i, "defect");

				$original_part = mysql_result ($result, $i, "original_part");

				$part_needed = mysql_result ($result, $i, "part_needed");

				$whyNotNeeded = mysql_result ($result, $i, "whyNotNeeded");

				$custname = mysql_result ($result, $i, "custname");

				$tech = mysql_result ($result, $i, "tech");

				$date = mysql_result ($result, $i, "date");

				$verify = mysql_result ($result, $i, "verify");

				$verify_date = mysql_result ($result, $i, "verify_date");

				$testbed = mysql_result ($result, $i, "testbed");

				}

			}

		else{echo "Error processing your request, please try again later.";}

		// Get Vendor and RMA Details

		$query = "SELECT * FROM eval_disp_rma WHERE id = '$eng_id' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$rma_vendor = mysql_result ($result, $i, "vendor");

				$rma_rma = mysql_result ($result, $i, "rma");

				$rma_dm = mysql_result ($result, $i, "dm");

				$rma_dm_amount = mysql_result ($result, $i, "dm_amount");

				$rma_shipdate = mysql_result ($result, $i, "shipdate");

				$rma_returningfor = mysql_result ($result, $i, "returningfor");

				}

			}

		else{echo "Error processing your request, please try again later.";}

		// Get Vendor and RMA Details

		$query = "SELECT * FROM eval_disp_ship WHERE id = '$eng_id' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$ship_rec_date = mysql_result ($result, $i, "rec_date");

				$ship_sn_rec = mysql_result ($result, $i, "sn_rec");

				$ship_part_is = mysql_result ($result, $i, "part_is");

				}

			}

		else{echo "Error processing your request, please try again later.";}

		// Get Customer RMA Number

		$query = "SELECT id FROM requests WHERE Serial_Number = '$gaibc' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$rma_number = mysql_result ($result, $i, "id");

				}

			}

		else{echo "Error processing your request, please try again later.";}

		//Get GAI Notes

		if ($rma_dm==''){$rma_dm='TBD';}

		$query = "SELECT note FROM vendor WHERE id='$eng_id'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$note = mysql_result ($result, $i, "note");

			}//end for

		}//end if

		else{  echo "Error processing your request, please try again later."; }

	?>

	<form action="submit_vend_receive_edit.php" method="post" enctype="multipart/form-data">

		<input name="fmchk" type="hidden" value="true" />

		<input name="id" type="hidden" value="<?php echo"$eng_id";?>" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="4"><span class="head-text">Edit Vendor RMA Receiving Details</span></th>

			</tr>

			<tr>

				<td colspan="4"><span class="small-red">This section is to be filled out by the Accounting Department</span></td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td>GAI&nbsp;Part&nbsp;BC#:</td>

				<td><?php echo"$gaibc";?></td>

				<td>Manufacturer:</td>

				<td><?php echo"$mfr";?></td>

			</tr>

			<tr>

				<td>GAI&nbsp;Part&nbsp;#:</td>

				<td><?php echo"$gaipn";?></td>

				<td>Model&nbsp;#:</td>

				<td><?php echo"$model";?></td>

			</tr>

			<tr>

				<td>System&nbsp;Serial&nbsp;#:</td>

				<td><?php echo"$verify_date";?></td>

				<td>Mfr Serial #:</td>

				<td><?php echo"$mfr_sn";?></td>

			</tr>

			<tr>

				<td valign="top">Defects&nbsp;Found:</td>

				<td colspan="3"><?php echo"$defect";?></td>

			</tr>

			<tr>

				<td>Origin&nbsp;of&nbsp;Part: </td>

				<td colspan="3">

					<?php

					if($original_part == "customer"){

						echo"<strong>CUSTOMER</strong>";

						}

					elseif($original_part == "vendor"){

						echo"<strong>VENDOR</strong>";

						}

					elseif($original_part == "stock"){

						echo"<strong>STOCKROOM</strong>";

						}

					?>

				</td>

			</tr>

			<tr>

				<td>Replacement&nbsp;Purchase&nbsp;Needed?</td>

				<td colspan="3">

					<?php

					if($part_needed == "yes"){

						echo"<strong>YES</strong>";

						}

					else {

						echo"<strong>NO</strong>";

						}

					?>

				</td>

			</tr>

			<tr>

				<td>Reason&nbsp;(If&nbsp;'No'):</td>

				<td colspan="3"><?php echo"$whyNotNeeded";?></td>

			</tr>

			<tr>

				<td>Customer&nbsp;Name:</td>

				<td colspan="3"><?php echo"$custname";?></td>

			</tr>

			

			<tr>

				<td>Customer&nbsp;RMA&nbsp;#:</td>

				<td colspan="3"><?php echo"$verify";?></td>

			</tr>

			<tr>

				<td align='left'>Eng / Tech:</td>

				<td align='left'><?php echo"$tech";?></td>

				<td align='left'>Date:</td>

				<td align='left'><?php echo"$date";?></td>

			</tr>

			<tr>

				<td colspan='4'>

					<hr align="center" width="100%" size="1" />

				</td>

			</tr>

			<tr>

				<td>Vendor:</td>

				<td><?php echo"$rma_vendor";?></td>

				<td>Returning For:</td>

				<td rowspan="5">

					<?php

					if($rma_returningfor == "credit"){ echo"

						<img src='/rma/images/box_on.png'>Credit<br />

						<img src='/rma/images/box_off.png'>Replacement<br />

						<img src='/rma/images/box_off.png'>Repair<br />

						<img src='/rma/images/box_off.png'>Scrap";

					}

					elseif($rma_returningfor == "replacement"){ echo"

						<img src='/rma/images/box_off.png'>Credit<br />

						<img src='/rma/images/box_on.png'>Replacement<br />

						<img src='/rma/images/box_off.png'>Repair<br />

						<img src='/rma/images/box_off.png'>Scrap";

					}

					elseif($rma_returningfor == "repair"){ echo"

						<img src='/rma/images/box_off.png'>Credit<br />

						<img src='/rma/images/box_off.png'>Replacement<br />

						<img src='/rma/images/box_on.png'>Repair<br />

						<img src='/rma/images/box_off.png'>Scrap";

					}

					elseif($rma_returningfor == "scrap"){ echo"

						<img src='/rma/images/box_off.png'>Credit<br />

						<img src='/rma/images/box_off.png'>Replacement<br />

						<img src='/rma/images/box_off.png'>Repair<br />

						<img src='/rma/images/box_on.png'>Scrap";

					}

					?>

				</td>

			</tr>

			<tr>

				<td>Vendor&nbsp;RMA&nbsp;#: </td>

				<td colspan="2"><?php echo"$rma_rma";?></td>

			</tr>

			<tr>

				<td>Debit&nbsp;Memo&nbsp;#: </td>

				<td colspan="2"><?php echo"$rma_dm";?></td>

			</tr>

			<tr>

				<td>Debit&nbsp;Amount:</td>

				<td colspan="2"><?php if ($rma_dm_amount != ""){echo"$$rma_dm_amount";} else echo"N/A";?></td>

			</tr>

			<tr>

				<td>Ship&nbsp;Date:</td>

				<td colspan="2"><?php echo"$rma_shipdate";?></td>

			</tr>

			<tr>

				<td valign="top">Additional Notes:</td>

				<td valign="top" colspan="3"><?php echo"<em>$note</em>";?></td>

			</tr>

			<tr>

				<td colspan='4'>

					<hr align="center" width="100%" size="1" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td>Date Received: </td>

				<td colspan="3"><input name="rec_date" type="text" id="rec_date" size="25" maxlength="200" <?php echo"value='$ship_rec_date'";?>/></td>

			</tr>

			<tr>

				<td>Serial# Received: </td>

				<td colspan="3"><input name="sn_rec" type="text" id="sn_rec" size="25" maxlength="30" <?php echo"value='$ship_sn_rec'";?>/></td>

			</tr>

			<tr>

				<td>Part Received Was: </td>

				<td colspan="2">

					<label><input type="radio" name="part_is" value="replacement" <?php if($ship_part_is == "replacement"){echo"checked='checked'";}?> />Replacement</label>

					<label><input type="radio" name="part_is" value="repair" <?php if($ship_part_is == "repair"){echo"checked='checked'";}?> />Repair</label></td>

				<td>&nbsp;</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td colspan="4" align="center">

				<input type="submit" class="form-button" name="Submit" value="Submit" />

				<br />

				<br />

				</td>

			</tr>

		</table>

	</form>

</body>

</html>
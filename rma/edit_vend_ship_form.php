<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 08.23.2007 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>RMA Shipping Details Editing</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

	<?php

		$gaibc = $_POST['gaibc'];

		if ($gaibc == ''){$gaibc = $_GET['gaibc'];}

		$editexisting = $_GET['editexisting'];

		include("/home/globalam/public_html/includes/configure.php") ;

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

			echo mysql_errno().": ".mysql_error()."<br />";

			exit;

		}//end if

		//Get barcode number for item currently editing

		$query = "SELECT * FROM eval_disp_eng WHERE gaibc = '$gaibc' ";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$eng_id = mysql_result ($result, $i, "id");

				$qty = mysql_result ($result, $i, "qty");

				$mfr = mysql_result ($result, $i, "mfr");

				$gaipn = mysql_result ($result, $i, "gaipn");

				$model = mysql_result ($result, $i, "model");

				$gaibc = mysql_result ($result, $i, "gaibc");

				$mfr_sn = mysql_result ($result, $i, "mfr_sn");

				$defect = mysql_result ($result, $i, "defect");

				$original_part = mysql_result ($result, $i, "original_part");

				$part_needed = mysql_result ($result, $i, "part_needed");

				$whyNotNeeded = mysql_result ($result, $i, "whyNotNeeded");

				$custname = mysql_result ($result, $i, "custname");

				$tech = mysql_result ($result, $i, "tech");

				$date = mysql_result ($result, $i, "date");

				$verify = mysql_result ($result, $i, "verify");

				$verify_date = mysql_result ($result, $i, "verify_date");

				$testbed = mysql_result ($result, $i, "testbed");

				}

			}

		else{echo "Error processing your request, please try again later.";}

		//Check to see if the data has been edited already

		$query = "SELECT * FROM eval_disp_rma WHERE id = '$eng_id'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$isEdited = mysql_result ($result, $i, "id");

				$vendor = mysql_result ($result, $i, "vendor");

				$rma = mysql_result ($result, $i, "rma");

				$dm = mysql_result ($result, $i, "dm");

				$dm_amount = mysql_result ($result, $i, "dm_amount");

				$shipdate = mysql_result ($result, $i, "shipdate");

				$returningFor = mysql_result ($result, $i, "returningfor");

			}//end for

		}//end if

		$query = "SELECT note FROM vendor WHERE id='$eng_id'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$note = mysql_result ($result, $i, "note");

			}//end for

		}//end if

		else{  echo "Error processing your request, please try again later."; }

		if ($isEdited != "" && $editexisting != 'true'){

			echo "<br /><span class='big-blue'>There is already an entry for this Bar Code</span><br /><br />

				<strong>Do you want to <a href='edit_vend_ship_form.php?gaibc=$gaibc&amp;editexisting=true'> Edit this Bar Code </a>

				<br />

				<br />or <br />

				<br />

				You can <a href='javascript:history.back()'>Check the Number and Try Again</a></strong>

				</body>

				</html>";

			exit;

		}//end if

	?>	

	<form action="submit_vend_ship_edit.php" method="post" enctype="multipart/form-data">

	<input name="fmchk" type="hidden" value="true" />

	<input name="editexisting" type="hidden" value="<?php echo "$editexisting";?>" />

	<input name="id" type="hidden" value="<?php echo"$eng_id"; ?>" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="4"><span class="head-text">Edit Vendor RMA Shipping Details</span> </th>

			</tr>

			<tr>

				<td colspan="4"><span class="small-red">This section is to be filled out by the Accounting Department</span></td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td>GAI&nbsp;Part&nbsp;BC#:</td>

				<td><?php echo"$gaibc";?></td>

				<td>Manufacturer:</td>

				<td><?php echo"$mfr";?></td>

			</tr>

			<tr>

				<td>GAI&nbsp;Part&nbsp;#:</td>

				<td><?php echo"$gaipn";?></td>

				<td>Model&nbsp;#:</td>

				<td><?php echo"$model";?></td>

			</tr>

			<tr>

				<td>System&nbsp;Serial&nbsp;#:</td>

				<td><?php echo"$verify_date";?></td>

				<td>Mfr Serial #:</td>

				<td><?php echo"$mfr_sn";?></td>

			</tr>

			<tr>

				<td valign="top">Defects&nbsp;Found:</td>

				<td colspan="3"><?php echo"$defect";?></td>

			</tr>

			<tr>

				<td>Origin&nbsp;of&nbsp;Part: </td>

				<td colspan="3">

					<?php

					if($original_part == "customer"){

						echo"<strong>CUSTOMER</strong>";

						}

					elseif($original_part == "vendor"){

						echo"<strong>VENDOR</strong>";

						}

					elseif($original_part == "stock"){

						echo"<strong>STOCKROOM</strong>";

						}

					?>

				</td>

			</tr>

			<tr>

				<td>Replacement&nbsp;Purchase&nbsp;Needed?</td>

				<td colspan="3">

					<?php

					if($part_needed == "yes"){

						echo"<strong>YES</strong>";

						}

					else {

						echo"<strong>NO</strong>";

						}

					?>

				</td>

			</tr>

			<tr>

				<td>Reason&nbsp;(If&nbsp;'No'):</td>

				<td colspan="3"><?php echo"$whyNotNeeded";?></td>

			</tr>

			<tr>

				<td>Customer&nbsp;Name:</td>

				<td colspan="3"><?php echo"$custname";?></td>

			</tr>

			

			<tr>

				<td>Customer&nbsp;RMA&nbsp;#:</td>

				<td colspan="3"><?php echo"$verify";?></td>

			</tr>

			<tr>

				<td align='left'>Eng / Tech:</td>

				<td align='left'><?php echo"$tech";?></td>

				<td align='left'>Date:</td>

				<td align='left'><?php echo"$date";?></td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td valign="top">Additional Notes:</td>

				<td valign="top" colspan = '3'><?php echo"<em>$note</em>";?></td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td colspan='4'>

					<hr align="center" width="100%" size="1" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td>Vendor: </td>

				<td>

					<input name="vendor" type="text" id="vendor" size="25" maxlength="200" value="<?php echo"$vendor";?>" />

				</td>

				<td>Vendor&nbsp;RMA&nbsp;#: </td>

				<td>

				<input name="rma" type="text" id="rma" size="25" maxlength="100" value="<?php echo"$rma";?>" />

				</td>

			</tr>

			<tr>

				<td>Debit&nbsp;Memo&nbsp;#: </td>

				<td>

					<input name="dm" type="text" id="dm" size="25" maxlength="30" value="<?php echo"$dm";?>" />

				</td>

				<td>Ship Date:</td>

				<td>

					<input name="shipdate" type="text" id="shipdate" size="25" maxlength="250" value="<?php echo"$shipdate";?>" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td>Returning For:</td>

				<td>

					<label>

						<input type="radio" name="returningfor" value="credit" <?php if ($returningFor =='credit'){echo "checked='checked'";}?> />Credit

					</label>

					<br />

					<label>

						<input type="radio" name="returningfor" value="replacement" <?php if ($returningFor =='replacement'){echo "checked='checked'";}?> />Replacement

					</label>

				</td>

				<td colspan="2">

					<label>

						<input type="radio" name="returningfor" value="repair" <?php if ($returningFor =='repair'){echo "checked='checked'";}?> />Repair

					</label>

					<br />

					<label>

						<input type="radio" name="returningfor" value="scrap" <?php if ($returningFor =='scrap'){echo "checked='checked'";}?> />Scrap - Out of Warranty

				</label>

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

			<tr>

				<td colspan="4" align='center'>

					<input class="form-button" type="submit" name="Submit" value="Submit" />

				</td>

			</tr>

			<tr>

				<td colspan="4">&nbsp;</td>

			</tr>

		</table>

	</form>

</body>

</html>
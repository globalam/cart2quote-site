<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 01.16.2008 -->
<head>
	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
	<title>Select Serial Number</title>
	<link rel="stylesheet" href="/rma/marcstyle.css" />
</head>
<body>

<?php
$name_1=$_POST['name_1'];
$email_1=$_POST['email_1'];
$name_2=$_POST['name_2'];
$email_2=$_POST['email_2'];
$name_3=$_POST['name_3'];
$email_3=$_POST['email_3'];
$rma=$_POST['rma'];
$rec_date=$_POST['rec_date'];
$cust=$_POST['cust_company'];
$pn=$_POST['Part_Number'];
$mailall=$_POST['mailall'];
$nocust=$_POST['nocust'];
$nomail=$_POST['nomail'];

//Email to First Name
$subject = "RMA# $rma received by Global American, Inc";
$message = "
Dear $name_1,

This is to inform you that we are in receipt of your product on RMA Number $rma.  Your product is currently being evaluated by one of our lab technicians

Please note that all items returned to Global American, Inc. must be returned in the same condition they were purchased including cables and accessories.  A full assessment of the failure may not be possible if all items purchased are not returned and may result in reoccurrence of this failure if all items cannot be evaluated for proper function.  It is our goal to complete testing, evaluation and repair of each item in a timely manner; however some products may require repair at our offshore factory.  Any repairs needing to be done at our offshore factory will incur a lead time of (6) to (8) weeks.  

All RMA returns will ship back UPS Ground and you will automatically be e-mailed the UPS tracking information upon shipment of your product.  All other shipment methods and fees will be assumed by the customer and must be indicated prior to shipment.

To expedite the RMA on any offshore repairs, Global American, Inc. would like to suggest purchasing a new item preventing unforeseen and/or unnecessary shortages.  Your RMA item once repaired/replaced and returned, may be kept as a spare or backup for your future requirements.  For further information on purchasing additional items please call 1-800-833-8999 Ext.301.  Should you at anytime have questions or require additional information concerning your RMA status, please contact our RMA Help Line 1-800-833-8999 Ext. 306 / 603-886-3900 Ext. 306.  

Global American, Inc. is flexible and willing to work with its customers to help them succeed in today's challenging High Tech World. If you have any questions, please contact us and we will be glad to assist.

Thank you very much for your business, and have a wonderful day!

Sincerely,

Global American, Inc's RMA Team

1-800-833-8999 Ext. 306

 ";
 //Test to see if message should be sent
if($mailall == 'true' && $nomail != 'true'){
	$mail = mail("$email_1", "$subject", "$message", "From:Global American RMA Dept. <rma@globalamericaninc.com>");
}//end if
//Email to Second Name
if($email_2 !=""){
$subject2 = "RMA# $rma received by Global American, Inc";
$message2 = "
Dear $name_2, 

This is to inform you that we are in receipt of your product on RMA Number $rma.  Your product is currently being evaluated by one of our lab technicians

Please note that all items returned to Global American, Inc. must be returned in the same condition they were purchased including cables and accessories.  A full assessment of the failure may not be possible if all items purchased are not returned and may result in reoccurrence of this failure if all items cannot be evaluated for proper function.  It is our goal to complete testing, evaluation and repair of each item in a timely manner; however some products may require repair at our offshore factory.  Any repairs needing to be done at our offshore factory will incur a lead time of (6) to (8) weeks.  

All RMA returns will ship back UPS Ground and you will automatically be e-mailed the UPS tracking information upon shipment of your product.  All other shipment methods and fees will be assumed by the customer and must be indicated prior to shipment.

To expedite the RMA on any offshore repairs, Global American, Inc. would like to suggest purchasing a new item preventing unforeseen and/or unnecessary shortages.  Your RMA item once repaired/replaced and returned, may be kept as a spare or backup for your future requirements.  For further information on purchasing additional items please call 1-800-833-8999 Ext.301.  Should you at anytime have questions or require additional information concerning your RMA status, please contact our RMA Help Line 1-800-833-8999 Ext. 306 / 603-886-3900 Ext. 306.  

Global American, Inc. is flexible and willing to work with its customers to help them succeed in today's challenging High Tech World. If you have any questions, please contact us and we will be glad to assist.

Thank you very much for your business, and have a wonderful day!

Sincerely,

Global American, Inc's RMA Team

1-800-833-8999 Ext. 306

 ";
if($mailall == 'true' && $nomail != 'true'){
	$mail2 = mail("$email_2", "$subject2", "$message2", "From:Global American RMA Dept. <rma@globalamericaninc.com>");
  }//end if
}//end if
//Email to Third Name
if($email_3 !=""){
$subject3 = "RMA# $rma received by Global American, Inc";
$message3 = "
Dear $name_3, 

This is to inform you that we are in receipt of your product on RMA Number $rma.  Your product is currently being evaluated by one of our lab technicians

Please note that all items returned to Global American, Inc. must be returned in the same condition they were purchased including cables and accessories.  A full assessment of the failure may not be possible if all items purchased are not returned and may result in reoccurrence of this failure if all items cannot be evaluated for proper function.  It is our goal to complete testing, evaluation and repair of each item in a timely manner; however some products may require repair at our offshore factory.  Any repairs needing to be done at our offshore factory will incur a lead time of (6) to (8) weeks.  

All RMA returns will ship back UPS Ground and you will automatically be e-mailed the UPS tracking information upon shipment of your product.  All other shipment methods and fees will be assumed by the customer and must be indicated prior to shipment.

To expedite the RMA on any offshore repairs, Global American, Inc. would like to suggest purchasing a new item preventing unforeseen and/or unnecessary shortages.  Your RMA item once repaired/replaced and returned, may be kept as a spare or backup for your future requirements.  For further information on purchasing additional items please call 1-800-833-8999 Ext.301.  Should you at anytime have questions or require additional information concerning your RMA status, please contact our RMA Help Line 1-800-833-8999 Ext. 306 / 603-886-3900 Ext. 306.  

Global American, Inc. is flexible and willing to work with its customers to help them succeed in today's challenging High Tech World. If you have any questions, please contact us and we will be glad to assist.

Thank you very much for your business, and have a wonderful day!

Sincerely,

Global American, Inc's RMA Team

1-800-833-8999 Ext. 306

 ";
if($mailall == 'true' && $nomail != 'true'){
	$mail3 = mail("$email_3", "$subject3", "$message3", "From:Global American RMA Dept. <rma@globalamericaninc.com>");
  }//end if
}//end if

//Email Global RMA Dept
$gaiemail = "webforms@globalamericaninc.com";
if($email_1 !=""){$contact1 = "$name_1 ( $email_1 )"; }
if($email_2 !=""){$contact2 = "$name_2 ( $email_2 )"; }
if($email_3 !=""){$contact3 = "$name_3 ( $email_3 )"; }
$subject4 = "RMA# $rma received by Global American, Inc";
$message4 = "
GAI RMA Dept, 

This message is intended to notify you that we have received:

RMA#: $rma

Customer: $cust

GAI P/N: $pn


The customer has been notified by email at the following addresses:

$contact1

$contact2

$contact3

This message was generated automatically. Please do not respond.
 ";
if($nocust == 'true' && $nomail != 'true'){
	$mail4 = mail("$gaiemail", "$subject4", "$message4", "From:Global American RMA Dept. <rma@globalamericaninc.com>");
 }//end if
//display messages to user
echo "
	<br />
	<span class='big-red'>Message(s):</span>
	<br />
	";// End echo
	if($mailall == 'true' && $nomail != 'true'){
		echo "
			<span class='big-blue'>RMA status notification sent to:</span><br /><br />";
			if($name_1 !=''){echo "<span class='big-red'>1.) </span><span class='big-blue'>$name_1 ($email_1) </span><br />";}
			if($name_2 !=''){echo "<span class='big-red'>2.) </span><span class='big-blue'>$name_2 ($email_2) </span><br />";}
			if($name_3 !=''){echo "<span class='big-red'>3.) </span><span class='big-blue'>$name_3 ($email_3) </span><br />";}
	}//end if
	if($nocust == 'true' && $nomail != 'true'){
		echo"
		<br />
		<span class='big-blue'>Global American, Inc. RMA Dept. has also been notified!</span>
		<br />";
	}//end if
	if($nomail == 'true'){
	echo"
	<span class='big-red'>Alert!</span>
	<br />
	<span class='big-red'>No Email has been sent to $cust!</span>
	<span class='big-red'>No Email has been sent to Global American Staff!</span>
	";
}//end if
?>
</body>
</html>
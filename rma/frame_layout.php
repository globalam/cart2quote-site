<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 01.27.2008 -->
	<head>
		<title>RMA Database Tools</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<frameset cols="250,*">
		<frame name="left" title="Navigation" noresize="noresize" scrolling="no" frameborder="0" src="nav_menu.php" />
		<?php
		// If user logs on with Administrator credentials, redirect main frame to Admin Tools
		if ($myUserName == 'Admin') {
			echo"<frame name='main' title='Admin Tools' frameborder='0' src='admin_tools.php' />";
			}
		else {
			echo"<frame name='main' title='Main Window' frameborder='0' src='welcome.php' />";
			}
		?>
		<noframes>
			<body>
				<span class='big-blue'>You must have a browser that supports frames to view this site!
			</span class='big-blue'></body>
		</noframes>
	<frame src="UntitledFrame-1" /></frameset>
</html>
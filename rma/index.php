<?php
/*	If user clicks logout link they
	are directed here. This will destroy 
	the session id.
*/
$logout=$_GET[logout];
if ($logout == 'yes'){
	session_start();
	session_destroy();
}//end if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 01.24.2008 -->
	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
		<title>Global American RMA Login</title>
		<link rel="stylesheet" href="/rma/marcstyle.css" />
		<style type="text/css">
			td {background-color: #FFFFFF;}
  			.textbox {width:180px; font-size:14pt; border:ridge 2px}
		</style>
	</head>
	<body>
	<form id="login" method="post" action="check_login.php">
	<table width="350" frame="box" rules="none" cellpadding="2" cellspacing="0">
			<tr>
				<th colspan="2"><span class="head-text">Global American, Inc.</span></th>
			</tr>
			<tr>
				<th colspan="2"><span class="head-text">RMA Database Interface Website</span></th>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td align="right"><span class="big-blue">Username:</span></td>
				<td align="left">
				<input name="myUserName" type="text" class="textbox" id="myUserName" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><small><i>This is your GAI Domain User Name.</i></small></td>
			</tr>
			<tr>
				<td align="right"><span class="big-blue">Password:</span></td>
				<td align="left">
				<input name="mypassword" type="password" class="textbox" id="mypassword" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><small><i>This field is case sensitive.</i></small></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td align="center">
					<input class="form-button" type="submit" name="Submit" value="Log In" />
			</td>
			</tr>
		</table>			
		</form>

	</body>
</html>
<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 06.18.2008 -->
	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8"  />
		<title>RMA Database Interface Menu</title>
		<link rel="stylesheet" href="/rma/marcstyle.css" />
	</head>
	<body>
		<table width="210" frame='box' rules="none" cellpadding='0' cellspacing='0' >
			<tr>
				<td>
					<a href="http://www.globalamericaninc.com" target='_blank'>
					<img border="0" src="/rma/images/logo_banner_left.png"
					alt="Global American Inc." height="52" width="208" /></a>
					</td>
			</tr>
			<tr>
				<td align='center'>
					<font size="+1" color="#606060">RMA Tools v2.1</font>
				</td>
			</tr>
		</table>
		<table width="210" frame='box' rules="none" cellpadding='2' cellspacing='0' >
			<tr>
				<th valign='top'><span class='head-text'>Vendor RMA</span> </th>
			</tr>
			<tr>
				<td align='center'>
					<a href='/rma/reports/show_combinedopen_report.php' target='main'>View Combined RMA Report</a><br />
					<a href='/rma/reports/show_vendor_all_report.php' target='main'>View All Vendor RMA Report</a><br />
					<a href='/rma/reports/show_vendor_open_report.php' target='main'>View Open RMA Report</a><br />
					<a href='/rma/reports/show_vendor_closed_report.php' target='main'>View Closed RMA Report</a><br />
					<a href='/rma/req_analysis_report.php' target='main'>View/Print Failure Analysis</a><br />
					<a href='/rma/edit_analysis.php' target='main'>New/Edit Failure Analysis</a><br />
					<a href='/rma/req_notes_edit.php' target='main'>Add/Edit Notes</a><br />
					<a href='/rma/req_vend_ship_edit.php' target='main'>Enter Shipping Details</a><br />
					<a href='/rma/req_vend_receive_edit.php' target='main'>Enter Receiving Details</a><br />
				</td>
			</tr>
		</table>
		<table width="210" frame='box' rules="none" cellpadding='2' cellspacing='0' >
			<tr>
				<th valign="top"><span class="head-text">Customer RMA</span> </th>
			</tr>
			<tr>
				<td align='center'>
					<a href='/rma/reports/show_combinedopen_report.php' target='main'>View Combined RMA Report</a><br />
					<a href='/rma/reports/show_cust_all_report.php' target='main'>All Customer Requests</a><br />
					<a href='/rma/reports/show_cust_open_report.php' target='main'>Open Customer Requests</a><br />
					<a href='/rma/reports/show_cust_closed_report.php' target='main'>Closed Customer Requests</a><br />
					<a href='/rma/req_cust_request.php' target='main'>Lookup Customer Requests</a><br />
					<a href='/rma/req_cust_reference.php' target='main'>Lookup Customer Ref Number</a><br />
					<a href='/rma/req_new_tech.php' target='main'>New Technician Report</a><br />
					<a href='/rma/req_rma_tech_edit.php' target='main'>Edit Technician Report</a><br />
					<a href='/rma/req_tech_report.php' target='main'>View/Print Technician Report</a><br />
					<!--<a href='/rma/req_sales_report.php' target='main'>View/Print Sales Report</a><br />-->
					<a href='/rma/req_cust_ship_edit.php' target='main'>Edit Shipping Details</a><br />
                    <a href='/rma/req_existing_rma_edit.php' target='main'>Edit Existing RMA</a><br />
					<!--<a href='/rma/req_rma_sales_edit.php' target='main'>Edit RMA (Sales)</a><br /> -->
					<a href='/rma/req_rma_status.php' target='main'>RMA Status Summary</a><br />
				</td>
			</tr>
		</table>
			<table width='210' border='0' rules="none" cellpadding='0' cellspacing='0'>
			<tr>
				<td align='left' style='background-color: #FFFFFF'>
					<?php
						if ($myUserName == 'Admin') {
							echo"
								<a href='/rma/admin_tools.php' target='main'><strong>Admin</strong></a>
							";
							}// End if
						else {
							echo"
								<a href='/rma/welcome.php' target='main'><strong>Home</strong></a>
							";
						}// End else				
					?>
				</td>
                <td align='center' style='background-color: #FFFFFF'>
					<a href='/rma/about.php' target='main'><strong>About</strong></a>
				</td>
				<td align='right' style='background-color: #FFFFFF'>
					<a href='/rma/index.php?logout=yes' target='_top'><strong>Logout</strong></a>
				</td>
			</tr>
		</table>
	</body>
</html>
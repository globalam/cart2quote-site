<?php

/*	No XHTML to be found here so no W3C verification. 

	This script will handle calls from all vendor and 

	customer reports and export them to a downloadable

	Microsoft Excel file.

	01.29.2008 - Marc Meledandri

*/

// Get type of report to export

$reportType = $_GET['reportType'];

// Functions required for export to excel.

// Begin file marker

function xlsBOF() {

echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);

return;

}// End function

// End file marker

function xlsEOF() {

echo pack("ss", 0x0A, 0x00);

return;

}// End function

// Write numerical data to a cell

function xlsWriteNumber($Row, $Col, $Value) {

echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);

echo pack("d", $Value);

return;

}// End function

// Write non-numerical data to a cell

function xlsWriteLabel($Row, $Col, $Value ) {

$L = strlen($Value);

echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);

echo $Value;

return;

}// End function

// Get date for use in filename

$today = date('m.d.y');

// Connect to database.

include("/home/globalam/public_html/includes/configure.php") ;

$connection = mysql_connect ("", "$user", "$password");

if ($connection == false){

	echo mysql_errno().": ".mysql_error()."<br />";

	exit;

}//end if

// Generate Excel file for Open Vendor RMA Report

if ($reportType == 'vendorOpen'){

	$query = "SELECT * FROM vendor, eval_disp_eng WHERE eval_disp_eng.id = vendor.id AND vendor.stat != 'closed'  ORDER BY vendor";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","vendor");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=OpenVendorRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=mysql_field_name($fields, $i);

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if

// Generate Excel file for Closed Vendor RMA Report

if ($reportType == 'vendorClosed'){

	$query = "SELECT * FROM vendor, eval_disp_eng WHERE stat = 'closed' AND eval_disp_eng.id = vendor.id ORDER BY vendor";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","vendor");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=ClosedVendorRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=mysql_field_name($fields, $i);

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if

// Generate Excel file for All Vendor RMA Report

if ($reportType == 'vendorAll'){

	$query = "SELECT * FROM vendor, eval_disp_eng WHERE eval_disp_eng.id = vendor.id ORDER BY vendor";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","vendor");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=AllVendorRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=mysql_field_name($fields, $i);

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if

// Generate Excel file for Open Customer RMA Report

if ($reportType == 'customerOpen'){

	$query = "SELECT * FROM requests WHERE Emailed != 'Closed' ORDER BY id";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","requests");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=OpenCustomerRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=mysql_field_name($fields, $i);

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if

// Generate Excel file for Closed Customer RMA Report

if ($reportType == 'customerClosed'){

	$query = "SELECT * FROM requests WHERE Emailed = 'closed' ORDER BY id";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","requests");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=ClosedCustomerRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=mysql_field_name($fields, $i);

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if]

// Generate Excel file for All Customer RMA Report

if ($reportType == 'customerAll'){

	$query = "SELECT * FROM requests ORDER BY id";

	$result = mysql_db_query ("globalam_magento", $query);

	// Get field names from database for use as headers

	$fields = mysql_list_fields("globalam_magento","requests");

	// Count the table fields.

	$columns = mysql_num_fields($fields);

	// Declare header information

	header("Pragma: public");

	header("Expires: 0");

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	header("Content-Type: application/force-download");

	header("Content-Type: application/octet-stream");

	header("Content-Type: application/download");;

	header("Content-Disposition: attachment;filename=AllCustomerRMA.$today.xls ");

	header("Content-Transfer-Encoding: binary ");

	// Begin Excel file write

	xlsBOF();

	// Get the column headers from the table field names.

	for ($i = 0; $i < $columns; $i++) {

		$data=mysql_field_name($fields, $i);

		xlsWriteLabel(0,$i,$data);	

	}// End for

	$xlsRow = 1;

	// Put data records from mysql by while loop.

	while($data=mysql_fetch_array($result)){

		for ($i = 0; $i < $columns; $i++) {

			$cell=htmlentities(mysql_field_name($fields, $i));

			xlsWriteLabel($xlsRow,$i,$data[$cell]);

			}// End for

		$xlsRow++;

	}// End while

	xlsEOF();

	exit();

}// End if

?>


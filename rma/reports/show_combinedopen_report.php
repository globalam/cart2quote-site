<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 08.21.2007 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8"  />

	<title>Open RMA Report</title>

	<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

</head>

<body>

	<a href="/rma/reports/cust-vend_rma_report_open1.php">Export to MS Excel </a>

	<table width='100%' border='1' cellspacing='0' cellpadding='2'>

	<tr>

		<th colspan="8"><span class="head-text">Customer Open RMA Report</span> </th>

	</tr>

	<tr>

		<td><strong><small>&nbsp;RMA&nbsp;#&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Bar-Code&nbsp;#&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Company Name&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Status&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Part&nbsp;#&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Problem Description&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Date Issued&nbsp;</small></strong></td>

		<td><strong><small>&nbsp;Date Received&nbsp;</small></strong></td>

	</tr>

	<?PHP

	include("/home/globalam/public_html/includes/configure.php") ; 

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

	  echo mysql_errno().": ".mysql_error()."<BR>";

	  exit;

	}//end if

	$query = "SELECT * FROM requests WHERE Emailed != 'closed' ORDER by id" ;

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$id = mysql_result ($result, $i, "id");

			$company = mysql_result ($result, $i, "Company");

			$contact = mysql_result ($result, $i, "Contact");

			$st = mysql_result ($result, $i, "Street");

			$City = mysql_result ($result, $i, "City");

			$State = mysql_result ($result, $i, "State");

			$Zip = mysql_result ($result, $i, "Zip");

			$Country = mysql_result ($result, $i, "Country");

			$Phone = mysql_result ($result, $i, "Phone");

			$Email = mysql_result ($result, $i, "Email");

			$Part_Number = mysql_result ($result, $i, "Part_Number");

			$Quantity = mysql_result ($result, $i, "Quantity");

			$Serial_Number = mysql_result ($result, $i, "Serial_Number");

			$Invoice = mysql_result ($result, $i, "Invoice");

			$Invoice_Date = mysql_result ($result, $i, "Invoice_Date");

			$PO_Number = mysql_result ($result, $i, "PO_Number");

			$PO_Date = mysql_result ($result, $i, "PO_Date");

			$Problem = mysql_result ($result, $i, "Problem");

			$stat = mysql_result ($result, $i, "Emailed");

			$date = mysql_result ($result, $i, "date");

			$rec_date = mysql_result ($result, $i, "rec_date");

			$ship_date = mysql_result ($result, $i, "ship_date");

			//Limit the length of the description 

			$Problem2 = substr($Problem , 0, 125);

			if ($id !='0'){

			echo"

				<tr>

					<td><small>&nbsp;$id&nbsp;</small></td>

					<td><small>&nbsp;$Serial_Number&nbsp;</small></td>

					<td><small>&nbsp;$company&nbsp;</small></td>

					<td><small>&nbsp;$stat&nbsp;</small></td>

					<td><small>&nbsp;$Part_Number&nbsp;</small></td>

					<td><small>&nbsp;$Problem2&nbsp;</small></td>

					<td><small>&nbsp;$date&nbsp;</small></td>

					<td><small>&nbsp;$rec_date&nbsp;</small></td>	

				</tr>";

				}//endif

		}//end for

	}//end if

	else{echo "Trouble connecting to the database, try again later.";}?>

	

	</table>

	<p>&nbsp;</p>

	<table width='100%' border='1' cellspacing='0' cellpadding='2'>

		<tr>

			<th colspan="8"><span class="head-text">Vendor Open RMA Report</span></th>

		</tr>	

		<tr>

			 <td><strong><small>&nbsp;Vendor&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Debit&nbsp;Memo&nbsp;#&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Part&nbsp;#&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Description&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Ship&nbsp;Date&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Customer&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Status&nbsp;</small></strong></td>

			 <td><strong><small>&nbsp;Notes&nbsp;</small></strong></td> 

		</tr>

	<?PHP

	$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

		  echo mysql_errno().": ".mysql_error()."<BR>";

		  exit;

		}  

	$query = "SELECT * FROM vendor WHERE stat != 'closed' ORDER BY '$vendor'";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$id = mysql_result ($result, $i, "id");

			$vendor = mysql_result ($result, $i, "vendor");

			$dm = mysql_result ($result, $i, "dm");

			$pn = mysql_result ($result, $i, "pn");

			$des = mysql_result ($result, $i, "des");

			$ship_date = mysql_result ($result, $i, "ship_date");

			$cust = mysql_result ($result, $i, "cust");

			$stat = mysql_result ($result, $i, "stat");

			$note = mysql_result ($result, $i, "note");

			echo "

				<tr>

					<td><small>&nbsp;$vendor&nbsp;</small></td>

					<td><small>&nbsp;$dm&nbsp;</small></td>

					<td><small>&nbsp;$pn&nbsp;</small></td>

					<td><small>&nbsp;$des&nbsp;</small></td>

					<td><small>&nbsp;$ship_date&nbsp; </small></td>

					<td><small>&nbsp;$cust&nbsp;</small></td>

					<td><small>&nbsp;$stat&nbsp;</small></td>

					<td><small>&nbsp;$note&nbsp;</small></td>

				</tr>";

		}//end for

	}//end if

	else{echo "Trouble connecting to the database, try again later.";}?>

	</table>

</body>

</html>


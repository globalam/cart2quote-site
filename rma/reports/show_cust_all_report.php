<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.23.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Customer Closed RMA Report</title>

	<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

</head>

<body>

	<table width='100%' border='1' cellspacing='0' cellpadding='2'>

		<tr>

			<th colspan="21"><span class="head-text">ALL Customer RMA Report</span></th>

		</tr>

		<tr>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=id'>&nbsp;RMA&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=company'>&nbsp;Company&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=contact'>&nbsp;Contact&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=st'>&nbsp;Address&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=City'>&nbsp;City&nbsp;</a></strong>

			</td>	

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=State'>&nbsp;State&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Zip'>&nbsp;Zip&nbsp;Code&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Country'>&nbsp;Country&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Phone'>&nbsp;Phone&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Email'>&nbsp;Email&nbsp;Address&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Part_Number'>&nbsp;Part&nbsp;Number&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Quantity'>&nbsp;Quantity&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Serial_Number'>&nbsp;Serial&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Invoice'>&nbsp;Invoice&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Invoice_Date'>&nbsp;Invoice&nbsp;Date&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=PO_Number'>&nbsp;P.O.&nbsp;Number&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=PO_Date'>&nbsp;P.O.&nbsp;Date&nbsp;</a></strong>

			</td>

			<td align='center'><strong>&nbsp;Problem&nbsp;Disc&nbsp;</strong></td>

			

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Emailed'>&nbsp;Status&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=date'>&nbsp;Date&nbsp;Issued&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=rec_date'>&nbsp;Date&nbsp;Received&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=ship_date'>&nbsp;Date&nbsp;Shipped&nbsp;</a></strong>

			</td>

		</tr>

		<?php

		/* set the defalt sort to RMA */

		$default_sort = 'id';

		/* if order is not set then set it to a default value. Otherwise, 

		 * set it to what was passed in. */

		if (!isset ($_GET['order'])){

			$order = $default_sort;

		}//end if

		else {

			$order = $_GET['order'];

		}//end else

		include("/home/globalam/public_html/includes/configure.php") ;

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

			echo mysql_errno().": ".mysql_error()."<br />";

		exit;

		}//end if

		$query = "SELECT * FROM requests ORDER by $order";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$id = htmlentities(mysql_result ($result, $i, "id"));

				$company = htmlentities(mysql_result ($result, $i, "Company"));

				$contact = htmlentities(mysql_result ($result, $i, "Contact"));

				$st = htmlentities(mysql_result ($result, $i, "Street"));

				$City = htmlentities(mysql_result ($result, $i, "City"));

				$State = htmlentities(mysql_result ($result, $i, "State"));

				$Zip = htmlentities(mysql_result ($result, $i, "Zip"));

				$Country = htmlentities(mysql_result ($result, $i, "Country"));

				$Phone = htmlentities(chunk_split(mysql_result ($result, $i, "Phone"),20));

				$Email = htmlentities(chunk_split(mysql_result ($result, $i, "Email"),40));

				$Part_Number = htmlentities(mysql_result ($result, $i, "Part_Number"));

				$Quantity = htmlentities(mysql_result ($result, $i, "Quantity"));

				$Serial_Number = htmlentities(mysql_result ($result, $i, "Serial_Number"));

				$Invoice = htmlentities(mysql_result ($result, $i, "Invoice"));

				$Invoice_Date = htmlentities(mysql_result ($result, $i, "Invoice_Date"));

				$PO_Number = htmlentities(mysql_result ($result, $i, "PO_Number"));

				$PO_Date = htmlentities(mysql_result ($result, $i, "PO_Date"));

				$Problem = htmlentities(chunk_split(mysql_result ($result, $i, "Problem"),50));

				$Emailed = htmlentities(mysql_result ($result, $i, "Emailed"));		

				$date = htmlentities(mysql_result ($result, $i, "date"));

				$rec_date = htmlentities(mysql_result ($result, $i, "rec_date"));

				$ship_date = htmlentities(mysql_result ($result, $i, "ship_date"));

				echo "

				<tr>

					<td><small>&nbsp;<a href='/rma/reports/show_cust_request_report.php?rma=$id&amp;bc=$Serial_Number'>$id</a></small></td>

					<td><small>&nbsp;$company</small></td>

					<td><small>&nbsp;$contact</small></td>

					<td><small>&nbsp;$st</small></td>

					<td><small>&nbsp;$City</small></td>

					<td><small>&nbsp;$State</small></td>

					<td><small>&nbsp;$Zip</small></td>

					<td><small>&nbsp;$Country</small></td>

					<td><small>&nbsp;$Phone</small></td>

					<td><small>&nbsp;$Email</small></td>

					<td><small>&nbsp;$Part_Number</small></td>

					<td><small>&nbsp;$Quantity</small></td>

					<td><small>&nbsp;$Serial_Number</small></td>

					<td><small>&nbsp;$Invoice</small></td>

					<td><small>&nbsp;$Invoice_Date</small></td>

					<td><small>&nbsp;$PO_Number</small></td>

					<td><small>&nbsp;$PO_Date</small></td>

					<td><small>&nbsp;$Problem</small></td>

					<td><small>&nbsp;$Emailed</small></td>

					<td><small>&nbsp;$date</small></td>

					<td><small>&nbsp;$rec_date</small></td>

					<td><small>&nbsp;$ship_date</small></td>	

				</tr>

				";//end echo

			}//end for

		}//end if

		else{

		echo "Trouble connectint to the database, try agaian later.";

		}//end else

		?>

	</table>

	<table>

		<tr>

			<td style="background-color:white;" colspan="3">&nbsp;</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right">

				<a href='javascript:printPage()'><img

				src="/rma/images/print.png"

				alt="Print This Page" height="40" width="34" /></a>

				&nbsp;&nbsp;&nbsp;&nbsp;

			</td>

			<td style="background-color:white;" width="30">&nbsp;</td>

			<td style="background-color:white;" align="left">

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<a href='/rma/reports/export_report_excel.php?reportType=customerAll'><img

				src="/rma/images/excel.png"

				alt="Export to Excel" height="40" width="34" /></a>

			</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right"><strong><a href='javascript:printPage()'>Print Page</a></strong></td>

			<td style="background-color:white;" width="30">&nbsp; </td>

			<td style="background-color:white;" align="left"><strong><a href='/rma/reports/export_report_excel.php?reportType=customerAll'>Create Excel</a></strong></td>

		</tr>

	</table>

</body>

</html>
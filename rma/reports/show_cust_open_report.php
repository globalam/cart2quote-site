<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.28.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Customer Open RMA Report</title>

	<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

</head>

<body>

	<table width='100%' border='1' cellspacing='0' cellpadding='2'>

		<tr>

			<th colspan="12"><span class="head-text"><span class="big-blue">OPEN</span> Customer RMA Report</span></th>

		</tr>

		<tr>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=requests.id'>&nbsp;RMA&nbsp;#&nbsp;</a></strong>

			</td>
            
            <td align='center'>

				<strong>&nbsp;Status&nbsp;</strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Serial_Number'>&nbsp;BC&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=company'>&nbsp;Company&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Emailed'>&nbsp;Current&nbsp;Status&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Part_Number'>&nbsp;Part&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;Warranty&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;Expiration&nbsp;</a></strong>

			</td>	

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=Invoice_date'>&nbsp;Invoiced&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;Problem&nbsp;Desc&nbsp;</strong>

			</td>

            <td align='center'>

				<strong>&nbsp;OS&nbsp;</strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=requests.date'>&nbsp;Date&nbsp;Issued&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=rec_date'>&nbsp;Received&nbsp;</a></strong>

			</td>
            

		</tr>

		<?php

		/* set the defalt sort to RMA */

		$default_sort = 'requests.id';

		/* if order is not set then set it to a default value. Otherwise, 

		 * set it to what was passed in. */

		if (!isset ($_GET['order'])){

			$order = $default_sort;

		}//end if

		else {

			$order = $_GET['order'];

		}//end else

		include("/home/globalam/public_html/includes/configure.php") ;

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

			echo mysql_errno().": ".mysql_error()."<br />";

		exit;

		}//end if

		$query = "SELECT id, Serial_Number, Company, Emailed, Part_Number, Invoice_Date, Problem, os, date, rec_date, approved FROM requests WHERE Emailed != 'Closed' ORDER BY $order";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$id = htmlentities(mysql_result ($result, $i, "id"));

				$Serial_Number = htmlentities(mysql_result ($result, $i, "Serial_Number"));

				$company = htmlentities(mysql_result ($result, $i, "Company"));

				$stat = mysql_result ($result, $i, "Emailed");				

				$Part_Number = htmlentities(mysql_result ($result, $i, "Part_Number"));

				$Invoice_Date = htmlentities(mysql_result ($result, $i, "Invoice_Date"));

				$Problem = mysql_result ($result, $i, "Problem");

				$os = mysql_result ($result, $i, "os");

				$date = mysql_result ($result, $i, "date");

				$rec_date = mysql_result ($result, $i, "rec_date");
				
				$approved = mysql_result ($result, $i, "approved");
				
				if ($approved == 3) { $statcolor = "#F00";}
				elseif ($approved == 2) { $statcolor = "#0F0";}
				elseif ($approved == 1) { $statcolor = "#FF0";}
				else {$statcolor = "#E8E8E8";}
				
				
				
				

				//Limit the length of the description 

				$Problem2 = substr($Problem , 0, 125);

				$query2 = "SELECT warranty, war_ex_date FROM tech_report WHERE sn_bc = '$Serial_Number'";

				$result2 = mysql_db_query ("globalam_magento", $query2);

				if ($result2){

					$numOfRows2 = mysql_num_rows ($result2);

					$warranty="";

					$war_ex_date="";

					for ($x = 0; $x < $numOfRows2; $x++){

						$warranty= mysql_result ($result2, $x, "warranty");

						$war_ex_date = mysql_result ($result2, $x, "war_ex_date");

					}// End for

					if ($warranty != "Yes" AND $warranty != "No"){$warranty = "Unknown";}

					if ($war_ex_date == ""){$war_ex_date = "Unknown";}

				}// End if

				else{

					$warranty="Unknown";

					$war_ex_date="Unknown";

				}

				if ($Invoice_Date ==""){$Invoice_Date = "Unknown";}

				echo "

				<tr>

					<td><small>&nbsp;<a href='/rma/reports/show_cust_request_report.php?rma=$id&amp;bc=$Serial_Number'>$id</a>&nbsp;</small></td>
					
					<td style='background: $statcolor;'><small>&nbsp;&nbsp;</small></td>

					<td><small>&nbsp;<a href='/rma/reports/show_tech_report.php?rma=$id&amp;bc=$Serial_Number'>$Serial_Number</a>&nbsp;</small></td>

					<td><small>&nbsp;$company&nbsp;</small></td>

					<td class='notWrapped'><small>&nbsp;$stat&nbsp;</small></td>

					<td><small>&nbsp;$Part_Number&nbsp;</small></td>

					<td><small>&nbsp;$warranty&nbsp;</small></td>

					<td><small>&nbsp;$war_ex_date&nbsp;</small></td>

					<td><small>&nbsp;$Invoice_Date&nbsp;</small></td>				

					<td><small>$Problem2&nbsp;</small></td>

					<td><small>$os&nbsp;</small></td>

					<td><small>&nbsp;$date&nbsp;</small></td>

					<td><small>&nbsp;$rec_date&nbsp;</small></td>	

				</tr>

				 ";

			}//end for

		}//end if

		else{

			echo "Please try again later.";

			echo mysql_errno().": ".mysql_error()."<br />";

		}//end else

		?>

	</table>

	<table>

		<tr>

			<td style="background-color:white;" colspan="3">&nbsp;</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right">

				<a href='javascript:printPage()'><img

				src="/rma/images/print.png"

				alt="Print This Page" height="40" width="34" /></a>

				&nbsp;&nbsp;&nbsp;&nbsp;

			</td>

			<td style="background-color:white;" width="30">&nbsp;</td>

			<td style="background-color:white;" align="left">

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<a href='/rma/reports/export_report_excel.php?reportType=customerOpen'><img

				src="/rma/images/excel.png"

				alt="Export to Excel" height="40" width="34" /></a>

			</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right"><strong><a href='javascript:printPage()'>Print Page</a></strong></td>

			<td style="background-color:white;" width="30">&nbsp; </td>

			<td style="background-color:white;" align="left"><strong><a href='/rma/reports/export_report_excel.php?reportType=customerOpen'>Create Excel</a></strong></td>

		</tr>

	</table>

</body>

</html>


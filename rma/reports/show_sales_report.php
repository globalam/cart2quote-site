<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.15.2008 -->

	<head>

		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

		<title>Sales Report</title>

		<link rel="stylesheet" href="/rma/marcstyle.css" />

		<script type="text/javascript">

			function printPage() {

				if (window.print) {agree = confirm('OK to print now?');

				if (agree) window.print();

				}

			}

		</script>

		<style type="text/css">

			td {background-color: #FFFFFF;}

		</style>

	</head>

	<body>

	<?PHP

	$sn_bc = $_POST['sn_bc'];

	if($sn_bc == ""){$sn_bc = $_GET['sn_bc'];}

	include("/home/globalam/public_html/includes/configure.php") ;

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

		echo mysql_errno().": ".mysql_error()."<br>";

		exit;

	}

	

	// Pull form data

	$query = "SELECT * FROM tech_report where  sn_bc = '$sn_bc' ";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$id = mysql_result ($result, $i, "id");

			$date = mysql_result ($result, $i, "date");

			$rma = mysql_result ($result, $i, "rma");

			$sn_bc = mysql_result ($result, $i, "sn_bc");

			$cust = mysql_result ($result, $i, "cust");

			$tech = mysql_result ($result, $i, "tech");

			$warranty = mysql_result ($result, $i, "warranty");

			$war_ex_date = mysql_result ($result, $i, "war_ex_date");

			$accessories = mysql_result ($result, $i, "accessories");

			$missing = mysql_result ($result, $i, "missing");

			$damage = mysql_result ($result, $i, "damage");

			$problem = mysql_result ($result, $i, "problem");

			$resolution = mysql_result ($result, $i, "resolution");

			$proposed = mysql_result ($result, $i, "proposed");

			$pics = mysql_result ($result, $i, "pics");

			$tech_time = mysql_result ($result, $i, "tech_time");

			$cost = mysql_result ($result, $i, "cost");

			$verify = mysql_result ($result, $i, "verify");

			$verify_date = mysql_result ($result, $i, "verify_date");

		}//end for

	}//end if

	else{  echo "Error processing your request, please try again later."; }

	

	//RMA Request Database pull

	$query = "SELECT Company , Part_Number, Serial_Number, Problem FROM requests where id = '$rma' ";

	$result = mysql_db_query ("globalam_magento", $query);

	if ($result){

		$numOfRows = mysql_num_rows ($result);

		for ($i = 0; $i < $numOfRows; $i++){

			$pn = mysql_result ($result, $i, "Part_Number");

			$sn = mysql_result ($result, $i, "Serial_Number");

			$reported_problem = mysql_result ($result, $i, "Problem");

		}//end for

	}//end if

	else{  echo "Error processing your request, please try again later."; }

	?>

	

	<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

		<tr>

			<th colspan="4"><span class="head-text">Sales Report (Customer RMA)</span></th>

		</tr>

		<tr>

			<td><strong>Date:  </strong><?PHP echo"$date"; ?></td>

			<td><strong>RMA#: &nbsp; </strong><?PHP echo"$rma"; ?></td>

			<td colspan="2"><strong>System SN# / Item Barcode#: </strong><?PHP echo"$sn_bc"; ?></td>

		</tr>

		<tr>

			<td colspan="2"><strong>Customer: </strong><?PHP echo"$cust"; ?></td>

			<td colspan="2"><strong>Technician: </strong><?PHP echo"$tech"; ?></td>

		</tr>

		<tr>

			<td colspan="2">

				<strong>Under Warranty: </strong>

				<?php if($warranty == "Yes"){echo "Yes";}

				if($warranty == "No"){echo "No";} ?> 

			</td>

			<td colspan="2" valign="top"><strong>Warranty Expires: </strong><?PHP echo"$war_ex_date"; ?></td>

		</tr>

		<tr>

			<td colspan='4'>

				<hr align="center" width="100%" size="1" />

			</td>

		</tr>

		<tr>

			<td colspan="4"><strong>Reported Defective Item:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$pn"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Accessories Included:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$accessories"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Missing Accessories:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$missing"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Damage:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$damage"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Issues Reported by Customer:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$reported_problem"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Issues Noted by Global American Technician:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$problem"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Resolution for Issues Noted:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$resolution"; ?></td>

		</tr>

		<tr>

			<td colspan="4"><strong>Suggested Corrective Action:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$proposed"; ?></td>

		</tr>

  		<tr>

			<td colspan='4'>

				<hr align="center" width="100%" size="1" />

			</td>

		</tr>

		<tr>

			<td colspan="4"><strong>Associated Costs:</strong></td>

		</tr>

		<tr>

			<td colspan="4"><?PHP echo"$pics"; ?></td>

		</tr>

		<tr>

			<td><strong>Technician Time: </strong></td>

			<td><?PHP echo"$tech_time"; ?></td>

			<td><strong>Cost: </strong></td>

			<td><?PHP echo"$cost"; ?></td>

		</tr>

		<tr>

			<td><strong>Verfied By: </strong></td>

			<td><?PHP echo"$verify"; ?></td>

			<td><strong>Date: </strong></td>

			<td><?PHP echo"$verify_date"; ?></td>

		</tr>

	</table>

	<br>

	<div align="center">

		<a href='javascript:printPage()'><img

		src="/rma/images/print.png"

		alt="Print This Page" height="40" width="34" /></a>

	</div>

	<div align="center">

		<strong><a href='javascript:printPage()'>Print Page</a></strong>

	</div>

</body>

</html>
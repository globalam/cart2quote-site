<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 01.18.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Open RMA Report</title>

	<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

</head>

<body>

	<table width='100%' border='1' cellspacing='0' cellpadding='2'>

		<tr>

			<th colspan="9"><span class="head-text"><span class="big-red">CLOSED</span> Vendor RMA Report</span></th>

		</tr>

		<tr>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=vendor'>Vendor&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=gaibc'>Bar&nbsp;Code&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=dm'>Debit&nbsp;Memo&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=pn'>Part&nbsp;#&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=des'>Description&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=ship_date'>Ship&nbsp;Date&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=cust'>Customer&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=stat'>Status&nbsp;</a></strong>

			</td>

			<td align='center'>

				<strong>&nbsp;<a href='<?php echo"{$_SERVER['PHP_SELF']}";?>?order=note'>Notes&nbsp;</a></strong>

			</td>

		</tr>

		<?php

		/* set the defalt sort to RMA */

		$default_sort = 'vendor';

		/* if order is not set then set it to a default value. Otherwise, 

		 * set it to what was passed in. */

		if (!isset ($_GET['order'])){

			$order = $default_sort;

		}//end if

		else {

			$order = $_GET['order'];

		}//end else

			include("/home/globalam/public_html/includes/configure.php") ;

			$connection = mysql_connect ("", "$user", "$password");

			if ($connection == false){

				echo mysql_errno().": ".mysql_error()."<br />";

				exit;

			}//end if

			$query = "SELECT * FROM vendor, eval_disp_eng WHERE stat = 'closed' AND eval_disp_eng.id = vendor.id ORDER BY $order";

			$result = mysql_db_query ("globalam_magento", $query);

			if ($result){

				$numOfRows = mysql_num_rows ($result);

				for ($i = 0; $i < $numOfRows; $i++){

					$id = mysql_result ($result, $i, "id");

					$vendor = htmlentities(mysql_result ($result, $i, "vendor"));

					$dm = mysql_result ($result, $i, "dm");

					$pn = mysql_result ($result, $i, "pn");

					$des = htmlentities(mysql_result ($result, $i, "des"));

					$ship_date = mysql_result ($result, $i, "ship_date");

					$cust = htmlentities(mysql_result ($result, $i, "cust"));

					$stat = mysql_result ($result, $i, "stat");

					$note = mysql_result ($result, $i, "note");

					$gaibc = chunk_split(mysql_result ($result, $i, "gaibc"),15);//limit data to 15 char/line

					echo"

					<tr>

						<td><small>&nbsp;$vendor&nbsp;</small></td>

					<td><small>&nbsp;<a href='/rma/reports/show_analysis_report.php?gaibc=$gaibc'>$gaibc</a>&nbsp;</small></td>

						<td><small>&nbsp;$dm&nbsp;</small></td>

						<td><small>&nbsp;$pn&nbsp;</small></td>

						<td><small>$des</small></td>

						<td><small>&nbsp;$ship_date&nbsp;</small></td>

						<td><small>&nbsp;$cust&nbsp;</small></td>

						<td><small>&nbsp;$stat&nbsp;</small></td>

						<td><small>&nbsp;$note&nbsp;</small></td>

					</tr>

					";

				}//end for

			}//end if

			else{

				echo "Trouble connecting to the database, try again later.";

			}//end else

		?>

	</table>

	<table>

		<tr>

			<td style="background-color:white;" colspan="3">&nbsp;</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right">

				<a href='javascript:printPage()'><img

				src="/rma/images/print.png"

				alt="Print This Page" height="40" width="34" /></a>

				&nbsp;&nbsp;&nbsp;&nbsp;

			</td>

			<td style="background-color:white;" width="30">&nbsp;</td>

			<td style="background-color:white;" align="left">

				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

				<a href='/rma/reports/export_report_excel.php?reportType=vendorClosed'><img

				src="/rma/images/excel.png"

				alt="Export to Excel" height="40" width="34" /></a>

			</td>

		</tr>

		<tr>

			<td style="background-color:white;" align="right"><strong><a href='javascript:printPage()'>Print Page</a></strong></td>

			<td style="background-color:white;" width="30">&nbsp; </td>

			<td style="background-color:white;" align="left"><strong><a href='/rma/reports/export_report_excel.php?reportType=vendorClosed'>Create Excel</a></strong></td>

		</tr>

	</table>

</body>

</html>
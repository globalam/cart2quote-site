<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 08.20.2007 -->
<head>
	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
	<title>Global American RMA Login</title>
	<link rel="stylesheet" href="/rma/marcstyle.css" />
</head>
<body>
	<form action="/rma/reports/show_analysis_report.php" method="post">
		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>
			<tr>
				<th><span class="head-text">Request RMA Analysis Report </span> </th>
			</tr>
			<tr>
				<td align='center'>				
					<br />
					<span class="title-text">Enter the Bar Code # for the Analysis Report: </span>					
					<br />
					<br />	
					<input name="fmchk" type="hidden" value="true" />
					<input name="gaibc" type="text" id="gaibc" size="25" />
					<br />
					<br />
					<input class="form-button" type="submit" name="Submit" value="Submit" />
					<br />&nbsp;	
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
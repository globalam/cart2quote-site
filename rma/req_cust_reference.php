<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 08.20.2007 -->
<head>
	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
	<title>Request Customer Reference Number</title>
	<link rel="stylesheet" href="/rma/marcstyle.css" />
</head>
<body>
	<form action="/rma/reports/show_tech_report.php" method="post">
		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>
			<tr>
				<th><span class="head-text">Request Technician Report </span> </th>
			</tr>
			<tr>
				<td align='center'>				
					<br />
					<span class="title-text">Enter the Customer Reference Number: </span>					
					<br />
					<br />	
					<input name="custRefNum" type="text" id="custRefNum" size="25" />
					<br />
					<br />
					<input class="form-button" type="submit" name="Submit" value="Submit" />
					<br />&nbsp;	
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
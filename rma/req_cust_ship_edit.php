<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 02.19.2008 -->

	<head>

		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

		<title>Edit RMA Shipping Details</title>

		<link rel="stylesheet" href="/rma/marcstyle.css" />

	</head>

	<body>

		<form action="req_cust_ship_edit2.php" method="post" >

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th><span class="head-text">Request RMA Number</span> </th>

			</tr>

			<tr>

				<td colspan='4' class='small-red' align='left'><span >This section to be completed by Shipping</span></td>

			</tr>

			<tr>

				<td>&nbsp;</td>

			</tr>

			<tr>

				<td align='center'><span class="title-text">Select the RMA Number: </span>

				<br />

				&nbsp;

				</td>

			</tr>

			<tr>

				<td align="center">

					<select name="rma">

						<option>RMA Number</option>

						<?php

						include("/home/globalam/public_html/includes/configure.php") ;

						$connection = mysql_connect ("", "$user", "$password");

						if ($connection == false){

							echo mysql_errno().": ".mysql_error()."<br />";

							exit;

						}//end if

						// Pull form data

						$query = "SELECT id, Company FROM requests ORDER BY id";

						$result = mysql_db_query ("globalam_magento", $query);

						if ($result){

							$numOfRows = mysql_num_rows ($result);

							for ($i = 0; $i < $numOfRows; $i++){

								$id = mysql_result ($result, $i, "id");

								$company = mysql_result ($result, $i, "company");

								$company = htmlentities($company);//Deal with special characters in Company Names								

								echo" <option value='$id'>$id - $company </option>";

							}//end for

						}//end if

						else {echo "Error processing your request, please try again later.";}

						?>

					</select>

					<br />

					&nbsp;

				</td>

			</tr>

			<tr>

				<td align="center">

					<input class="form-button" type="submit" name="Submit" value="Submit" />

					<br />&nbsp;

				</td>

			</tr>

		</table>

	</form>

</body>

</html>
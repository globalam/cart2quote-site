<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 01.10.2008 -->
	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
		<title>Request New Technician Report</title>
		<link rel="stylesheet" href="/rma/marcstyle.css" />
	</head>
	<body>
		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>
			<tr>
				<th><span class="head-text">New Technician RMA Report </span> </th>
			</tr>
			<tr>
				<td colspan='4' class='small-red' align='left'><span >This section to be completed by Engineering </span></td>
			</tr>
			<tr>
				<td align='center'>
					<form action="edit_new_tech.php" method="post" enctype="multipart/form-data">
					<p>
					<span class="title-text">Enter an RMA # to begin Report: </span>
					<br />
					<br />	
					<input name="fmchk" type="hidden" value="true" />
					<input name="gairma" type="text" id="gaibc" size="25" />
					<br />
					<br />
					<input class="form-button" type="submit" name="Submit" value="Submit" />
					<br />&nbsp;
					</p>
					</form>
				</td>
			</tr>
		</table>
	</body>
</html>
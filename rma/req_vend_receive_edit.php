<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 05.16.2008 -->

	<head>

		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

		<title>Vendor RMA Receiving Details Request</title>

		<link rel="stylesheet" href="/rma/marcstyle.css" />

	</head>

	<body>

	<form action="edit_vend_receive_form.php" method="post" enctype="multipart/form-data">

		<input name="fmchk" type="hidden" value="true" />

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th><span class="head-text">Request RMA Receiving Details</span> </th>

			</tr>

			<tr>

				<td colspan="2"><span class="small-red">This section is to be filled out by the Shipping Department</span></td>

			</tr>

			<tr>

				<td align='center'>

					<br />

					<span class="title-text">Select the Bar Code of the Item to Update:</span>

					<br />

					<br />

					<select name="gaibc">

						<option>Select Bar Code</option>

						<?php

						include("/home/globalam/public_html/includes/configure.php") ;

						//Check to make sure that the serial number exists in Engineering table

						$connection = mysql_connect ("", "$user", "$password");

						if ($connection == false){

							echo mysql_errno().": ".mysql_error()."<br />";

							exit;

						}//endif

						// Pull form data

						$query = "SELECT gaibc FROM eval_disp_eng ORDER BY gaibc DESC";

						$result = mysql_db_query ("globalam_magento", $query);

						if ($result){

							$numOfRows = mysql_num_rows ($result);

							for ($i = 0; $i < $numOfRows; $i++){

								$gaibc = mysql_result ($result, $i, "gaibc");

								echo "<option value='$gaibc'>$gaibc</option>";

							}//end for

						}//end if

						?>

					</select>

				</td>

			</tr>

			<tr>

				<td colspan="2">&nbsp;</td>

			</tr>

			<tr>

				<td align='center'>

					<input class="form-button" type="submit" name="Submit" value="Submit" />

				</td>

			</tr>

			<tr>

				<td colspan="2">&nbsp;</td>

			</tr>

		</table>

	</form>

</body>

</html>
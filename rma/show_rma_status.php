<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 05.14.2008 -->

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>RMA Status Summary</title>

	<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

</head>

<body>

	<?php

		// Barcode requested for Status Summary

		$gaibc = $_POST['gaibc'];

		// Open Database Connection

		include("/home/globalam/public_html/includes/configure.php");

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

			echo mysql_errno().": ".mysql_error()."<br />";

			exit;

		}// End if

		// Get dates of RMA Request, Received from Customer and Shipped to Customer

		$query = "SELECT Company, date, rec_date, ship_date FROM requests WHERE Serial_Number = '$gaibc'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$company = htmlentities(mysql_result ($result, $i, "Company"));

				$requestDate = htmlentities(mysql_result ($result, $i, "date"));

				$inFromCustomer = htmlentities(mysql_result ($result, $i, "rec_date"));

				$outToCustomer = htmlentities(mysql_result ($result, $i, "ship_date"));

			}//end for

		}//end if

		else{

			echo "Please try again later.";

		}//end else

		// Get date of Technician Report

		$query = "SELECT date FROM tech_report WHERE sn_bc = '$gaibc'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$techReportDate = htmlentities(mysql_result ($result, $i, "date"));

			}//end for

		}//end if

		else{

			echo "Please try again later.";

		}//end else

		// Get date of Failure Analysis

		$query = "SELECT id, date FROM eval_disp_eng WHERE gaibc = '$gaibc'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$referenceId = mysql_result ($result, $i, "id");

				$analysisDate = htmlentities(mysql_result ($result, $i, "date"));

			}//end for

		}//end if

		else{

			echo "Please try again later.";

		}//end else

		// Get Vendor Name and Date Shipped to Vendor for repair

		$query = "SELECT vendor, ship_date FROM vendor, eval_disp_eng WHERE eval_disp_eng.id = vendor.id AND gaibc = '$gaibc'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$vendor = htmlentities(mysql_result ($result, $i, "vendor"));

				$outToVendor = htmlentities(mysql_result ($result, $i, "ship_date"));

			}//end for

		}//end if

		else{

			echo "Please try again later.";

		}//end else

		// Get date Received Back From Vendor

		$query = "SELECT rec_date FROM eval_disp_ship WHERE id = '$referenceId'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

			$numOfRows = mysql_num_rows ($result);

			for ($i = 0; $i < $numOfRows; $i++){

				$inFromVendor = htmlentities(mysql_result ($result, $i, "rec_date"));

			}//end for

		}//end if

		else{

			echo "Please try again later.";

		}//end else

		?>

		<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<th colspan="2"><span class="head-text">RMA Status Summary</span></th>

			</tr>

			<tr>

				<td colspan="2">&nbsp;</td>

			</tr>

			<tr>

				<td width='300' height='30' align="right"><span class="title-text">Part Barcode #:&nbsp;&nbsp;</span></td>

				<td width='300' height='30' align="left"><span class="big-red"><?php echo "&nbsp;&nbsp;$gaibc"?></span></td>

			</tr>

			<tr>

				<td width='300' height='30' align="right"><span class="title-text">Customer Name:&nbsp;&nbsp;</span></td>

				<td width='300' height='30' align="left"><span class="big-red"><?php echo "&nbsp;&nbsp;$company"?></span></td>

			</tr>

			<tr>

				<td width='300' height='30' align="right"><span class="title-text">Vendor Name:&nbsp;&nbsp;</span></td>

				<td width='300' height='30' align="left"><span class="big-red"><?php echo "&nbsp;&nbsp;$vendor"?></span></td>

			</tr>

			<tr>

				<td colspan="2">&nbsp;</td>

			</tr>

		</table>

		<table width='600' frame='void' rules='none' cellpadding='2' cellspacing='0'>

			<tr>

				<td colspan="4" style="background-color:#FFFFFF;">&nbsp;</td>

			</tr>

		<?php

			if ($requestDate != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;1.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='250' height='40' align='right'><strong>Customer Request Date:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='250' height='40' align='left'><strong>&nbsp;&nbsp;$requestDate</span></strong></large></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;1.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Customer Request Date:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($inFromCustomer != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;2.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Received from Customer:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$inFromCustomer</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			<tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;2.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Received from Customer:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($techReportDate != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;3.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Technician Report Submitted:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$techReportDate</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;3.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Technician Report Submitted:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($analysisDate != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;4.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Failure Analysis Submitted:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$analysisDate</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;4.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Failure Analysis Submitted:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($outToVendor != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;5.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Shipped to Vendor:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$outToVendor</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;5.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Shipped to Vendor:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($inFromVendor != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;6.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Received from Vendor:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$inFromVendor</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;6.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Received from Vendor:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>

			<tr>

				<td colspan='2' style='border:thin; border-right-style:dashed; background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

				<td colspan='2' style='background-color:#FFFFFF' width='300' height='20'>&nbsp;</td>

			</tr>";

			}// End else

			

			if ($outToCustomer != ''){

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#33FF33'><strong>&nbsp;&nbsp;7.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#33FF33' width='300' height='40' align='right'><strong>Shipped to Customer:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#33FF33' width='300' height='40' align='left'><strong>&nbsp;&nbsp;$outToCustomer</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#33FF33' width='25' height='40' ><span class='title-text'>&radic;&nbsp;&nbsp;</span></td>

			</tr>";

			}// End if

			else {

			echo"

			<tr>

				<td width='25' height='40' style='border:thin; border-style:dashed; border-right-style:none; background-color:#FF3333'><strong>&nbsp;&nbsp;7.)</strong></td>

				<td style='border:thin; border-style:dashed; border-right-style:none; border-left-style:none; background-color:#FF3333' width='300' height='40' align='right'><strong>Shipped to Customer:&nbsp;&nbsp;</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; border-right-style:none; background-color:#FF3333' width='300' height='40' align='left'><strong>&nbsp;&nbsp;Unavailable</strong></td>

				<td style='border:thin; border-style:dashed; border-left-style:none; background-color:#FF3333' width='25' height='40' ><span class='title-text'><strong>X&nbsp;&nbsp;</strong></span></td>

			</tr>";

			}// End else

		?>

	</table>

</body>

</html>
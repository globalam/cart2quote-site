<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 02.21.2008 -->

	<head>

		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

		<title>Submit Engineering Analysis</title>

		<link rel="stylesheet" href="http://www.globalamericaninc.com/rma/marcstyle.css" />

	</head>

	<body>

	<?php

		$qty = 1;

		$mfr = $_POST['mfr'];

		$gaipn =$_POST['gaipn'];

		$model = $_POST['model'];

		$gaibc1 = $_POST['gaibc'];

		$gaibc = trim($gaibc1); 

		$mfr_sn = $_POST['mfr_sn'];

		$defect = ltrim($_POST['defect']);

		$origin_part = $_POST['origin_part'];

		$part_needed = $_POST['part_needed'];

		$whyNotNeeded = $_POST['whyNotNeeded'];

		$custname = $_POST['custname'];

		$tech = $_POST['tech'];

		$date = $_POST['date'];

		$verify = $_POST['verify'];

		$verify_date = $_POST['verify_date'];

		$testbed = ltrim($_POST['testbed']);

		$fmchk = $_POST['fmchk'];

		//Vendor Report Variables

		$vendor=$_POST['vendor'];

		$status=$_POST['status'];

		

		// Run form check

		if($fmchk != "true"){ echo"Request to modify database not authorized!</body></html>"; exit();}	

		// Get database login variables

		include("/home/globalam/public_html/includes/configure.php") ;

		// Insert data into tech table

		$connection = mysql_connect ("", "$user", "$password");

		if ($connection == false){

		  echo mysql_errno().": ".mysql_error()."<br />";

		  exit;

		}  

		mysql_select_db("globalam_magento");

		$query = "INSERT INTO eval_disp_eng ( id, qty, mfr, gaipn, model, gaibc, mfr_sn, defect, original_part, 

								part_needed, whyNotNeeded, custname, tech, date, verify, verify_date, testbed ) 

								VALUES( '', '$qty', '$mfr', '$gaipn', '$model', '$gaibc', '$mfr_sn', '$defect', 

								'$origin_part', '$part_needed', '$whyNotNeeded', '$custname', '$tech', '$date', '$verify', 

								'$verify_date', '$testbed' )";

			

		$result = mysql_query ($query) or die('Invalid query: ' . mysql_error());

		//Insert data into vendor table

		// Establish a few variables from form before entering into database

		// Insert data from form into the database

		mysql_connect ("", "$user", "$password");

		mysql_select_db("globalam_magento");

		$query = "INSERT INTO vendor ( id, vendor, dm, pn, des, ship_date, cust, stat, note) 

								VALUES( LAST_INSERT_ID(), '$vendor', '', '$gaipn', '$defect', '', '$custname', '$status', '' )";

		$result = mysql_query ($query) or DIE("Unable to record your vendor information");



		//Update Status

		// Get customer rma number from request table in the DB

		mysql_connect ("", "$user", "$password");

		$query = "SELECT id FROM requests WHERE Serial_Number='$gaibc'";

		$result = mysql_db_query ("globalam_magento", $query);

		if ($result){

		  $numOfRows = mysql_num_rows ($result);

		  for ($i = 0; $i < $numOfRows; $i++){

			$rma = mysql_result ($result, $i, "id");

		  }

		}

//Update Customer RMA Status if serial # has a customer RMA#

if($rma != ""){

	mysql_connect ("", "$user", "$password");

	mysql_select_db("globalam_magento");

	$connection = mysql_connect ("", "$user", "$password");

	if ($connection == false){

		echo mysql_errno().": ".mysql_error()."<br />";

		exit; 

	}// End if

	$query = "UPDATE requests SET Emailed='Failure Analysis Submitted' WHERE id='$rma' " ;

	$result = mysql_query ($query) or DIE(" <p><br />Unable to update Status ");

}// End if

//Email recipient

$gaiemail = "webforms@globalamericaninc.com";

$subject = "Engineering Analysis for Bar Code#: $gaibc";

$message = "

<table width='500'>

	<tr>

		<td colspan='2'><strong>GAI Engineering Department has submitted a</strong></td>

	</tr>

	<tr>

		<td colspan='2'><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vendor RMA Failure Analysis:</strong></td>

	</tr>

	<tr>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td>Bar Code#:</td>

		<td>$gaibc</td>

	</tr>

	<tr>

		<td>Global Part#:</td>

		<td>$gaipn</td>

	</tr>

	<tr>

		<td>System Serial#:</td>

		<td>$verify_date</td>

	</tr>

	<tr>

		<td>Manufacturer:</td>

		<td>$mfr</td>

	</tr>

	<tr>

		<td>Model:</td>

		<td>$model</td>

	</tr>

	<tr>

		<td>Mfr. Serial#:</td>

		<td>$mfr_sn</td>

	</tr>

	<tr>

		<td valign='top'>Defects Found:</td>

		<td>$defect</td>

	</tr>

	<tr>

		<td valign='top'>Testbed:</td>

		<td>$testbed</td>

	</tr>

	<tr>

		<td>Origin of Part:</td>

		<td>$origin_part</td>

	</tr>	

	<tr>

		<td valign='top'>Does&nbsp;Replacement&nbsp;Part&nbsp;<br />

			Need&nbsp;to&nbsp;be&nbsp;Purchased?</td>

		<td valign='top'>$part_needed</td>

	</tr>

	<tr>

		<td valign='top'>If&nbsp;No&nbsp;Please&nbsp;Explain:</td>

		<td valign='top'>$whyNotNeeded</td>

	<tr>

		<td>Customer&nbsp;Name:</td>

		<td>$custname</td>

	</tr>

	<tr>

		<td>Customer&nbsp;RMA#:</td>

		<td>$verify</td>

	</tr>	

	<tr>

		<td>Technician:</td>

		<td>$tech</td>

	</tr>

	<tr>

		<td>Date:</td>

		<td>$date</td>

	</tr>

	<tr>

		<td>Status:</td>

		<td>$status</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td colspan='2'>

			<strong>This Email has been automatically generated by Global American, Inc.</strong>

		</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td colspan='2'>

			<strong>Please Do Not Reply.</strong>

		</td>

	</tr>

</table>

</body>

</html>

";

$headers  = 'MIME-Version: 1.0' . "\r\n";

$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

$headers .= 'From: Global American RMA Dept.<rma@globalamericaninc.com>' . "\r\n";

$headers .= 'Cc: globalamericanmail@gmail.com' . "\r\n";

mail($gaiemail, $subject, $message, $headers);

?>

	<br />

	<span class='big-red'>Message(s):</span>

	<br />

	<br />

		<span class='big-blue'>Analysis Submission Successful!</span>

		<br />

		<br />

		You have submitted your analysis of Global American Part#:<strong><?php echo" $gaipn "?></strong>with the barcode #:<strong><?php echo" $gaibc"?></strong>

		<br />

		An email containing the information you submitted has been sent to the following recipients:<strong><?php echo" $gaiemail"?></strong>

		<br />

		<br /> 		

		Would you like to enter another Analysis for a product with the same defect(s)? 

		<strong>( <a href='submit_analysis_next.php?gaibc=<?php echo "$gaibc"?>'>YES</a> / </strong>

				<strong><a href='/rma/welcome.php'>NO</a> )</strong>

	</body>

</html>
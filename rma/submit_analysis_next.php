<?php

/* 	Check the session cookie to ensure the user

	is logged in. If not, boot them back to logon.

	Access to this page is blocked without proper credentials.

	Remove this php code block to check with W3C Validator!-MM

*/

session_start();

if(!session_is_registered(myUserName)){

	header("location:index.php");

}// End if

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--W3C Verified XHTML/CSS - Marc Meledandri 02.21.2008 -->

<head>

	<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />

	<title>Subsequent Analysis Submittals</title>

	<link rel="stylesheet" href="/rma/marcstyle.css" />

</head>

<body>

<?php

$gaibc = $_GET['gaibc'];

include("/home/globalam/public_html/includes/configure.php") ;

$connection = mysql_connect ("", "$user", "$password");

if ($connection == false){

	echo mysql_errno().": ".mysql_error()."<br />";

	exit;

}

$query = "select * from eval_disp_eng where gaibc = '$gaibc' ";

$result = mysql_db_query ("globalam_magento", $query);

if ($result){

	$numOfRows = mysql_num_rows ($result);

	for ($i = 0; $i < $numOfRows; $i++){

		$eng_id = mysql_result ($result, $i, "id");

		$qty = mysql_result ($result, $i, "qty");

		$mfr = mysql_result ($result, $i, "mfr");

		$gaipn = mysql_result ($result, $i, "gaipn");

		$model = mysql_result ($result, $i, "model");

		$gaibc = mysql_result ($result, $i, "gaibc");

		$mfr_sn = mysql_result ($result, $i, "mfr_sn");

		$defect = mysql_result ($result, $i, "defect");

		$original_part = mysql_result ($result, $i, "original_part");

		$part_needed = mysql_result ($result, $i, "part_needed");

		$whyNotNeeded = mysql_result ($result, $i, "whyNotNeeded");

		$custname = mysql_result ($result, $i, "custname");

		$tech = mysql_result ($result, $i, "tech");

		$date = mysql_result ($result, $i, "date");

		$verify = mysql_result ($result, $i, "verify");

		$verify_date = mysql_result ($result, $i, "verify_date");

		$testbed = mysql_result ($result, $i, "testbed");

		}

	}

else {echo "Error processing your request, please try again later.";}

//Second query

$query = "select vendor from vendor where id = '$eng_id' ";

$result = mysql_db_query ("globalam_magento", $query);

if ($result){

	$numOfRows = mysql_num_rows ($result);

	for ($i = 0; $i < $numOfRows; $i++){

		$vendor = mysql_result ($result, $i, "vendor");

		}

// End if

else{echo "Cannot get the status from Vendor (ID=$eng_id";}

?>

	<table width='600' frame='box' rules='none' cellpadding='2' cellspacing='0'>

		<tr>

			<td width='600' scope='col'>

				<form action="submit_analysis.php" method="post" enctype="multipart/form-data">

				<input name="fmchk" type="hidden" value="true" />

				<input name="status" type="hidden" value="Waiting To Ship" />

					<table border="0" cellspacing="2" cellpadding="2">

						<tr>

							<th class='head-text' colspan="4" > RMA Analysis - Engineering </th>

						</tr>

						<tr>

							<td colspan='4' class='small-red' align='left'><span >This section to be completed by Engineering </span></td>

						</tr>

						<tr>

							<td colspan="2" align='left'></td>

						</tr>

						<tr>

							<td>GAI Part BC #: </td>

							<td><input name='gaibc' type='text' id='gaibc' size='25' maxlength='100'/></td>



							<td>Manufacturer: </td>

							<td><input name="mfr" type="text" id="mfr" size="25" maxlength="250" value="<?php echo"$mfr";?>" /></td>

						</tr>

						<tr>

							<td>GAI Part #: </td>

							<td><input name="gaipn" type="text" id="gaipn" size="25" maxlength="30" value="<?php echo"$gaipn";?>" /></td>

							<td>Model #: </td>

							<td><input name="model" type="text" id="model" size="25" maxlength="100" value="<?php echo"$model";?>" /></td>

						</tr>

						<tr>

							<td>System Serial #: </td>

							<td><input name='verify_date' type='text' id='verify_date' size='25' maxlength='100' value="<?php echo"$verify_date";?>" /></td>

    					<td>Mfr. Serial #: </td>

							<td><input name="mfr_sn" type="text" id="mfr_sn" size="25" maxlength="100" value="<?php echo"$mfr_sn";?>" /></td>

						</tr>

						<tr>

							<td height="100" align='left'>Defects Found:</td>

							<td colspan="3" align='left'><textarea name="defect" cols="50" rows="5"><?php {echo"$defect";} ?></textarea></td>

							</tr>

						<tr>

							<td height="100" align='left'>Test Configuration:<br />

								<em class='small-text'>Include Details of Backplane, Power Supply, CPU, Memory and Operating System </em></td>

							<td colspan="3" align='left'>

								<textarea name="testbed" cols="50" rows="8" id="testbed"><?php {echo"$testbed";} ?></textarea></td>

						</tr>

						<tr>

							<td align='left'>Origin of Part: </td>

							<td colspan="3" align='left'>

								<label><input type="radio" name="origin_part" value="stock"  <?php if($original_part == 'stock'){echo"checked='checked'";} ?> />Stockroom</label>

								<br />

								<label><input type="radio" name="origin_part" value="vendor" <?php if($original_part == 'vendor'){echo"checked='checked'";} ?> />Vendor</label>

								<br />

								<label><input type="radio" name="origin_part" value="customer" <?php if($original_part == 'customer'){echo"checked='checked'";} ?> />Customer</label>

								<br />

							</td>

						</tr>

						<tr>

							<td colspan="4" align='left'>Does Replacement Part Need to be Purchased?  

								<input name="part_needed" type="checkbox" value="yes" <?php if($part_needed == 'yes'){echo"checked='checked'";} ?> />

								Yes |

								<input name="part_needed" type="checkbox" value="no" <?php if($part_needed != 'yes'){echo"checked='checked'";} ?> />

								No

							</td>

							</tr>

						<tr>

							<td>

								<font color="#FF0000">If 'No' Please Explain:</font>

							</td>

							<td colspan="3">

								<input name="whyNotNeeded" type="text" id="whyNotNeeded" size="40" maxlength="250" <?php echo"value='$whyNotNeeded'"; ?> />

							</td>

						</tr>

						<tr>

							<td align='left'><div align="left">Customer Name:<br />

								<span class='small-text'>(if Applicable)</span></div>

							</td>

							<td colspan="2" align='left'><input name="custname" type="text" id="custname" size="40" maxlength="250" <?php echo"value='$custname'";?> />

							</td>

						</tr>

						<tr>

							<td>Customer RMA #:<br />

								<span class='small-text'>(if Applicable)</span></td>

							<td colspan="2"><input name="verify" type="text" id="verify" size="25" maxlength="100" value="<?php echo"$verify";?>" /></td>

						</tr>

						<tr>

							<td align='left'>Technician Name:</td>

							<td align='left'><input name="tech" type="text" id="tech" size="25" maxlength="30" <?php echo"value='$tech'"; ?> /></td>

							<td align='left'>Date:</td>

							<td align='left'><input name="date" type="text" id="date" size="25" maxlength="30" <?php echo"value='$date'"; ?> /></td>

						</tr>

						<tr>

							<td colspan="4">

								<div align="center"><br />

									<input class="form-button" type="submit" name="Submit" value="Submit" />

									<br />

								</div>

							</td>

						</tr>

					</table>

				</form>

			</td>

		</tr>

	</table>

</body>

</html>
<?php
/* 	Check the session cookie to ensure the user
	is logged in. If not, boot them back to logon.
	Access to this page is blocked without proper credentials.
	Remove this php code block to check with W3C Validator!-MM
*/
session_start();
if(!session_is_registered(myUserName)){
	header("location:index.php");
}// End if
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!--W3C Verified XHTML/CSS - Marc Meledandri 08.17.2007 -->
	<head>
		<meta http-equiv="Content-Type" content="application/xhtml+xml;charset=utf-8" />
		<title>Global American Welcome</title>
		<link rel="stylesheet" href="marcstyle.css" />
	</head>
	<body>
		<br />
		<div align='center'>
			<span class='big-red'><?php date_default_timezone_set('US/Eastern'); echo date("F j, Y, g:i a")?></span>
		</div>
		<br />
		<div align='center'>
			<img src="/rma/images/busted.png"
					alt="Busted" height="200" width="280" />
		</div>
		<br />
	  <h1 align='center'>
	  	Welcome, <?php echo"$myUserName"?>, to the<br />
	  	Global American, Inc.<br />
		RMA Database Website!
	 	</h1>
	 	<br />
	 	<div align='center'>
	 		Not <?php echo"$myUserName"?>? <a href='/rma/index.php?logout=yes' target='top' title='Log In'>Click Here</a> to Log In again.
	 	</div>
	 	<br />
	 	<br />
	 	<br />
		 <div align='center'>
	 	<i><----- Click any link on the navigation pane at the left to get started!</i>
	 	</div>
	</body>
</html>